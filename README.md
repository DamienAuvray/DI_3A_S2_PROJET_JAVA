# Project Title

Modelisation et programmation objet : Projet encadre java

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install Eclipse (Oxygen.2 Release (4.7.2)).

### Installing

File>import>Exisiting Projects into Workspace

Then select the directory and click on finish

## Running app

### Application Centrale
package folder: src_app_centrale
(default package) AppCentrale.java

Model:
polytech.tours.di.projet.java.app.centrale.model
View:
polytech.tours.di.projet.java.app.centrale.view
Controller:
polytech.tours.di.projet.java.app.centrale.controller

### Application Pointeuse
package folder: src_pointeuse
(default package) AppPointeuse.java

Model:
polytech.tours.di.projet.java.pointeuse.model
View:
polytech.tours.di.projet.java.pointeuse.view
Controller:
polytech.tours.di.projet.java.pointeuse.controller

## Running the tests

Test package folder: src_app_centrale
polytech.tours.di.projet.java.app.centrale.test

## Deployment
 
Windows 10 (1803)

Application Centrale:

```
java -jar AppCentral.jar
```

Application Pointeuse:

```
java -jar AppPointeuse.jar
```


## Authors

* **Damien Auvray** - *Initial work* - [damienauvray.fr](https://damienauvray.fr)
