import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import polytech.tours.di.projet.java.app.centrale.controller.*;
import polytech.tours.di.projet.java.app.centrale.model.company.*;
import polytech.tours.di.projet.java.app.centrale.model.config.Config;
import polytech.tours.di.projet.java.app.centrale.model.time.*;
import polytech.tours.di.projet.java.app.centrale.view.*;

/**
 * @author Damien Auvray
 */
public class AppCentrale {
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Date ldt = new Date();
	    System.out.println(ldt);

	    Company company = new Company();
	    //company.stub();
	    
	    try {
	    	ObjectInputStream ois = new ObjectInputStream(new FileInputStream("company.ser"));
			company = (Company) ois.readObject();
			ois.close();
		} catch (ClassNotFoundException e) {
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		}
	    
	    Config.update();
	    
		AppCentraleView view = new AppCentraleView();
		AppCentraleController c = new AppCentraleController(company, view);
		c.initController();
	    
	}

}
