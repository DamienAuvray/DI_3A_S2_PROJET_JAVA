package polytech.tours.di.projet.java.app.centrale.controller;

import polytech.tours.di.projet.java.app.centrale.model.company.Company;
import polytech.tours.di.projet.java.app.centrale.model.company.Employee;
import polytech.tours.di.projet.java.app.centrale.model.company.Manager;
import polytech.tours.di.projet.java.app.centrale.view.AddDepartmentView;

/**
 * AddDepartmentController
 * @author Damien Auvray
 *
 */
public class AddDepartmentController {
	private Company model;
	private AddDepartmentView view;

	/**
	 * AddDepartmentController
	 * @param m
	 * @param v
	 */
	public AddDepartmentController( Company m, AddDepartmentView v){
		model = m;
		view = v;
		initView();
	}
	
	/**
	 * initView
	 */
	public void initView(){
		view.getCbManager().addItem("");
		view.getBtnClose().addActionListener(e -> view.dispose());
	}
	
	/**
	 * initController
	 */
	public void initController(){
		/* 
		for(Employee e : model.getEmployees()){
			if(e.getClass()==Manager.class){
				if(e.getClass()==Manager.class && !model.getLeader().contains(e) )
					view.getCbManager().addItem(e.getName() + " " + e.getFirstName());
			}
		}
		*/
	}
	
	/**
	 * addDepartment
	 */
	public void addDepartment(){
		if(view.getTxtNom().getText().length()!=0){
			model.addDepartment(view.getTxtNom().getText());
			for(Employee e: model.getEmployees()){
				if(view.getCbManager().getSelectedItem().toString().contains(e.getName()) &&
					view.getCbManager().getSelectedItem().toString().contains(e.getFirstName()))
					model.getDepartment(view.getTxtNom().getText()).setLeader((Manager) e);
			}
			view.dispose();
		}
	}

	/**
	 * getModel
	 * @return model
	 */
	public Company getModel() {
		return model;
	}

	/**
	 * setModel
	 * @param model
	 */
	public void setModel(Company model) {
		this.model = model;
	}

	/**
	 * AddDepartmentView
	 * @return view
	 */
	public AddDepartmentView getView() {
		return view;
	}

	/**
	 * setView
	 * @param view
	 */
	public void setView(AddDepartmentView view) {
		this.view = view;
	}
}
