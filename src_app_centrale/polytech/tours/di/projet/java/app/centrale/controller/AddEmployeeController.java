package polytech.tours.di.projet.java.app.centrale.controller;

import polytech.tours.di.projet.java.app.centrale.model.company.Company;
import polytech.tours.di.projet.java.app.centrale.model.time.Date;
import polytech.tours.di.projet.java.app.centrale.view.AddEmployeeView;

/**
 * AddEmployeeController
 * @author Damien Auvray
 *
 */
public class AddEmployeeController {
	
	private Company model;
	private AddEmployeeView view;

	/**
	 * AddEmployeeController
	 * @param m
	 * @param v
	 */
	public AddEmployeeController( Company m, AddEmployeeView v){
		model = m;
		view = v;
		initView();
	}
	
	/**
	 * initView
	 */
	public void initView(){
		view.getBtnClose().addActionListener(e -> view.dispose());
		view.getTxtArrivingDate().setText(new Date().toString());
		view.getTxtLeavingDate().setText(new Date().toString());
	}
	
	/**
	 * initController
	 */
	public void initController(){
	}
	
	/**
	 * addEmployee
	 */
	public void addEmployee(){
		boolean isValid=true;
		Date arriving = new Date();
		Date leaving = new Date();
		if(view.getTxtNom().getText().length()!=0 &&
			view.getTxtPrenom().getText().length()!=0 &&
			view.getTxtDepartment().getText().length()!=0 &&
			view.getTxtArrivingDate().getText().length()!=0 &&
			view.getTxtLeavingDate().getText().length()!=0	){
			
			try{
				arriving = new Date(view.getTxtArrivingDate().getText()); 
				leaving = new Date(view.getTxtLeavingDate().getText());
			}catch(Exception e){
				isValid=false;
			}
			if(isValid){
				if(view.getTxtEmail().getText().length() != 0)
					model.addManager(view.getTxtNom().getText(), view.getTxtPrenom().getText(), arriving, leaving, view.getTxtEmail().getText(), view.getTxtDepartment().getText());
				else
					model.addEmployee(view.getTxtNom().getText(), view.getTxtPrenom().getText(), arriving, leaving, view.getTxtDepartment().getText());
				view.dispose();
			}
		}
	}

	/**
	 * getModel
	 * @return Company model
	 */
	public Company getModel() {
		return model;
	}

	/**
	 * Company
	 * @param Company model
	 */
	public void Company(Company model) {
		this.model = model;
	}

	/**
	 * getView
	 * @return AddEmployeeView view
	 */
	public AddEmployeeView getView() {
		return view;
	}

	/**
	 * setView
	 * @param AddEmployeeView view
	 */
	public void setView(AddEmployeeView view) {
		this.view = view;
	}
	
	
}
