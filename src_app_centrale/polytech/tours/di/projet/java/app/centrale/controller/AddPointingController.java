package polytech.tours.di.projet.java.app.centrale.controller;

import polytech.tours.di.projet.java.app.centrale.model.company.*;
import polytech.tours.di.projet.java.app.centrale.model.time.Date;
import polytech.tours.di.projet.java.app.centrale.model.time.Pointing;
import polytech.tours.di.projet.java.app.centrale.view.AddPointingView;

/**
 * AddPointingController
 * @author Damien Auvray
 *
 */
public class AddPointingController {

	
	private Company model;
	private AddPointingView view;

	/**
	 * AddPointingController
	 * @param m
	 * @param v
	 */
	public AddPointingController( Company m, AddPointingView v){
		model = m;
		view = v;
		initView();
	}
	
	/**
	 * initView
	 */
	public void initView(){
		if(model.getEmployees() != null){
			for(Employee e : model.getEmployees()){
				view.getCbEmployee().addItem(e.getName() + " " + e.getFirstName());
			}
		}
		view.getTxtDate().setText(new Date().toString());
		view.getBtnClose().addActionListener(e -> view.dispose());
	}
	
	/**
	 * initController
	 */
	public void initController(){
	}
	
	/**
	 * addPointing
	 */
	public void addPointing(){
		boolean isValid=true;
		Date date = new Date();
		try{
			date = new Date(view.getTxtDate().getText()); 
		}catch(Exception e){
			isValid=false;
		}
		if(isValid){
			for(Employee e: model.getEmployees()){
				if(view.getCbEmployee().getSelectedItem().toString().contains(e.getName()) &&
					view.getCbEmployee().getSelectedItem().toString().contains(e.getFirstName())){
					model.addPointing(new Pointing(e,date));
					view.dispose();
				}
			}
		}
	}

	/**
	 * getModel
	 * @return model
	 */
	public Company getModel() {
		return model;
	}

	/**
	 * setModel
	 * @param model
	 */
	public void setModel(Company model) {
		this.model = model;
	}

	/**
	 * AddPointingView
	 * @return view
	 */
	public AddPointingView getView() {
		return view;
	}

	/**
	 * setView
	 * @param view
	 */
	public void setView(AddPointingView view) {
		this.view = view;
	}
}
