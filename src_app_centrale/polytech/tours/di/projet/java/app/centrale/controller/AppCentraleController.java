package polytech.tours.di.projet.java.app.centrale.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JTable;

import polytech.tours.di.projet.java.app.centrale.model.company.*;
import polytech.tours.di.projet.java.app.centrale.model.config.Config;
import polytech.tours.di.projet.java.app.centrale.model.tcp.TCPServerEmployee;
import polytech.tours.di.projet.java.app.centrale.model.tcp.TCPServerPointing;
import polytech.tours.di.projet.java.app.centrale.model.time.*;
import polytech.tours.di.projet.java.app.centrale.view.*;

/**
 * AppCentraleController
 * @author Damien Auvray
 *
 */
public class AppCentraleController{
	private Company model;
	private AppCentraleView view;
	
	private TCPServerEmployee tcpServerEmployee;
	private TCPServerPointing tcpServerPointing;
	
	/**
	 * getModel
	 * @return model
	 */
    public Company getModel() {
		return model;
	}

    /**
     * AppCentraleController
     * @param m
     * @param v
     */
	public AppCentraleController( Company m, AppCentraleView v){
		model = m;
		view = v;
		initView();
		tcpServerEmployee = new TCPServerEmployee(model);
		tcpServerPointing = new TCPServerPointing();
		new Thread(tcpServerEmployee).start();
		new Thread(tcpServerPointing).start();
	}
	
	/**
	 * initView
	 */
	public void initView(){	
		initTable();
		//INIT Table
		refreshTableData();
		
		//SEARCH
		view.getBtnSearchPointing().addActionListener(e -> searchPointing());
		view.getBtnSearchDepartment().addActionListener(e -> seachDepartment());
		view.getBtnSearchEmployee().addActionListener(e -> searchEmployee());
		
		//REFRESH
		view.getTp().addChangeListener(e ->	refreshTableData());
		view.getBtnRefreshPointing().addActionListener(e -> initTablePointing());
		view.getBtnRefreshDepartment().addActionListener(e -> initTableDepartment());
		view.getBtnRefreshEmployee().addActionListener(e -> initTableEmployee());
		view.getChckbxDaillyPointingsOnly().addActionListener(e -> initTablePointing());
		
		//IMPORT EXPORT
		view.getBtnImportCsv().addActionListener(e -> importCSV());
		view.getBtnExportCsv().addActionListener(e -> exportCSV());
		view.getBtnImportPointing().addActionListener(e -> pointingCSV());
		
		//SET CLOSE OPERATION
		view.getFrame().setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		view.getFrame().addWindowListener(new java.awt.event.WindowAdapter() {
		    @Override
		    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
		    	try {
		    		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("company.ser"));
		    		oos.writeObject(model);
		    		oos.close();
		    		Config.save();
				} catch (FileNotFoundException e) {
				} catch (IOException e) {
				}
		    	System.exit(0);
		    }
		});
		
	}
	
	/**
	 * initController
	 */
	public void initController(){		
		//ADD
		view.getBtnAddPointing().addActionListener(e -> addPointing());
		view.getBtnAddDepartment().addActionListener(e -> addDepartment());
		view.getBtnAddEmployee().addActionListener(e -> addEmployee());
		
		//UPDATE
		view.getTablePointing().addMouseListener(new MouseAdapter() {
		    public void mousePressed(MouseEvent mouseEvent) {
		        getPointing(mouseEvent);
		    }
		});
		
		view.getTableDepartment().addMouseListener(new MouseAdapter() {
		    public void mousePressed(MouseEvent mouseEvent) {
		       getDepartment(mouseEvent);
		    }
		});
		
		view.getTableEmployee().addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent mouseEvent) {
				getEmployee(mouseEvent);
		    }
		});
		view.getBtnUpdate().addActionListener(e -> updateConfig());
	}
	
	/**
	 * refreshTableData
	 */
	void refreshTableData(){
		initTablePointing();
		initTableDepartment();
		initTableEmployee();
		initConfig();
	}
	
	/**
	 * initTable
	 */
	void initTable(){
		view.getModeltablePointing().addColumn("Name");
		view.getModeltablePointing().addColumn("First Name");
		view.getModeltablePointing().addColumn("Date");
		view.getModeltablePointing().addColumn("Arriving/Leaving");
		view.getModeltableDepartment().addColumn("Department");
		view.getModeltableDepartment().addColumn("Manager (Name, FirstName)");
		view.getModeltableDepartment().addColumn("Count Employee");
		view.getModeltableEmployee().addColumn("Name");
		view.getModeltableEmployee().addColumn("First Name");
		view.getModeltableEmployee().addColumn("Department");
		view.getModeltableEmployee().addColumn("Arriving Date");
		view.getModeltableEmployee().addColumn("Leaving Date");
		view.getModeltableEmployee().addColumn("Overtime");
		view.getModeltableEmployee().addColumn("Manager");
	}
	
	/**
	 * initTablePointing
	 */
	void initTablePointing(){
		view.getModeltablePointing().setRowCount(0);
		if(tcpServerPointing!=null){
			for(Pointing p: tcpServerPointing.getPointing()){
				if(model.getEmployee(p.getEmployee().getName(), p.getEmployee().getFirstName())!=null)
						model.addPointing(new Pointing(model.getEmployee(p.getEmployee().getName(), p.getEmployee().getFirstName()),p.getDate()));
			}
			tcpServerPointing.getPointing().removeAll(tcpServerPointing.getPointing());
		}
		for(Day day : model.getDays()){
			if(day.getArrivingPointing()!=null){
				if(!view.getChckbxDaillyPointingsOnly().isSelected()||
						(view.getChckbxDaillyPointingsOnly().isSelected()&&
						day.getArrivingPointing().getDate().getLdt().getYear() == new Date().getLdt().getYear()&&
						day.getArrivingPointing().getDate().getLdt().getDayOfYear() == new Date().getLdt().getDayOfYear()))
				view.getModeltablePointing().addRow(new Object[]{
						day.getEmployee().getName(),
						day.getEmployee().getFirstName(),
						day.getArrivingPointing().getDate(),
						"Arriving " 
						+ 
						(day.getArrivingPointing().getDate().getLdt().getMinute()-day.getEmployee().getArrivingHour().getLdt().getMinute() + (day.getArrivingPointing().getDate().getLdt().getHour()-day.getEmployee().getArrivingHour().getLdt().getHour())*60)			
						+
						" min"
				});
			}
			if(day.getLeavingPointing()!=null){
				if(!view.getChckbxDaillyPointingsOnly().isSelected()||
						(view.getChckbxDaillyPointingsOnly().isSelected()&&
						day.getArrivingPointing().getDate().getLdt().getYear() == new Date().getLdt().getYear()&&
						day.getArrivingPointing().getDate().getLdt().getDayOfYear() == new Date().getLdt().getDayOfYear()))
				view.getModeltablePointing().addRow(new Object[]{
						day.getEmployee().getName(),
						day.getEmployee().getFirstName(),
						day.getLeavingPointing().getDate(),
						"Leaving " 
						+
						(day.getLeavingPointing().getDate().getLdt().getMinute()-day.getEmployee().getLeavingHour().getLdt().getMinute() + (day.getLeavingPointing().getDate().getLdt().getHour()-day.getEmployee().getLeavingHour().getLdt().getHour())*60)			
						+
						" min"
				});
			}
		}
	}
	
	/**
	 * initTableDepartment
	 */
	void initTableDepartment(){
		view.getModeltableDepartment().setRowCount(0);
		for(Department dep : model.getDepartment()){
			String leaderStr = "";
			if(dep.getLeader() != null)
				leaderStr = dep.getLeader().getName() +" "+ dep.getLeader().getFirstName();
			view.getModeltableDepartment().addRow(new Object[]{
					dep.getName(),
					leaderStr,
					dep.getEmployees().size()
			});
		}
	}
	
	/**
	 * initTableEmployee
	 */
	void initTableEmployee(){
		view.getModeltableEmployee().setRowCount(0);
		for(Employee emp : model.getEmployees()){
			view.getModeltableEmployee().addRow(new Object[]{
					emp.getName(),
					emp.getFirstName(),
					model.getDepartmentEmployee(emp).getName(),
					emp.getArrivingHour(),
					emp.getLeavingHour(),
					emp.getOvertime() + " min",
					emp.getClass().getSimpleName()
			});
		}
	}
	
	/**
	 * initConfig
	 */
	void initConfig(){
		view.getTxtIPAdress().setText(Config.getIpAdress());
		view.getTxtPortEmployee().setText(String.valueOf(Config.getPortServerEmployee()));
		view.getTxtPortPointing().setText(String.valueOf(Config.getPortServerPointing()));
		view.getTxtAccidentThreshold().setText(String.valueOf(Config.getAccidentThreshold()));
	}
	
	/**
	 * importCSV
	 */
	void importCSV(){
		JFileChooser fileChooser = new JFileChooser();
        int returnValue = fileChooser.showOpenDialog(null);
        if (returnValue == JFileChooser.APPROVE_OPTION) {
        	File selectedFile = fileChooser.getSelectedFile();
        	try {
        		model.importCSV(selectedFile);
        		initTableEmployee();
        	} catch (IOException e) {
        		e.printStackTrace();
        	}
        }
	}
	
	/**
	 * exportCSV
	 */
	void exportCSV(){
		JFileChooser fileChooser = new JFileChooser();
        int returnValue = fileChooser.showOpenDialog(null);
        if (returnValue == JFileChooser.APPROVE_OPTION) {
        	File selectedFile = fileChooser.getSelectedFile();
	        try {
	        	model.exportCSV(selectedFile);
			} catch (IOException e) {
				e.printStackTrace();
			}
        }
	}
	
	/**
	 * pointingCSV
	 */
	void pointingCSV(){
		JFileChooser fileChooser = new JFileChooser();
        int returnValue = fileChooser.showOpenDialog(null);
        if (returnValue == JFileChooser.APPROVE_OPTION) {
        	File selectedFile = fileChooser.getSelectedFile();
        	try {
        		model.pointingCSV(selectedFile);
        		initTablePointing();
        	} catch (IOException e) {
        		e.printStackTrace();
        	}
        }
	}

	/**
	 * addPointing
	 */
	void addPointing(){
		AddPointingView view = new AddPointingView();
		AddPointingController c = new AddPointingController(model, view);
		c.initController();
		c.getView().getBtnAdd().addActionListener(new ActionListener() { 
		    public void actionPerformed(ActionEvent e) {  
		    	c.addPointing();
		    	model=c.getModel();
		    	refreshTableData();
		    }
		});
	}
	
	/**
	 * addDepartment
	 */
	void addDepartment(){
		AddDepartmentView view = new AddDepartmentView();
		AddDepartmentController c = new AddDepartmentController(model, view);
		c.initController();
		c.getView().getBtnAdd().addActionListener(new ActionListener() { 
		    public void actionPerformed(ActionEvent e) {  
		    	c.addDepartment();
		    	model=c.getModel();
		    	refreshTableData();
		    }
		});
	}
	
	/**
	 * addEmployee
	 */
	void addEmployee(){
		AddEmployeeView view = new AddEmployeeView();
		AddEmployeeController c = new AddEmployeeController(model, view);
		c.initController();
		c.getView().getBtnAdd().addActionListener(new ActionListener() { 
		    public void actionPerformed(ActionEvent e) {  
		    	c.addEmployee();
		    	model=c.getModel();
		    	refreshTableData();
		    }
		});
	}
	
	/**
	 * searchEmployee
	 */
	void searchEmployee() { 
    	view.getModeltableEmployee().setRowCount(0);
		for(Employee emp : model.getEmployees()){
			if(emp.getName().contains(view.getTxtSearchEmployee().getText()) ||
			emp.getFirstName().contains(view.getTxtSearchEmployee().getText()) ||
			model.getDepartmentEmployee(emp).getName().contains(view.getTxtSearchEmployee().getText()) ||
			emp.getArrivingHour().toString().contains(view.getTxtSearchEmployee().getText()) ||
			emp.getLeavingHour().toString().contains(view.getTxtSearchEmployee().getText()) ||
			String.valueOf(emp.getOvertime()).contains(view.getTxtSearchEmployee().getText()) ||
			emp.getClass().getSimpleName().contains(view.getTxtSearchEmployee().getText())
			){
				view.getModeltableEmployee().addRow(new Object[]{
						emp.getName(),
						emp.getFirstName(),
						model.getDepartmentEmployee(emp).getName(),
						emp.getArrivingHour(),
						emp.getLeavingHour(),
						emp.getOvertime() + " min",
						emp.getClass().getSimpleName()
				});
			}
		}
    	
	} 
	
	/**
	 * seachDepartment
	 */
	void seachDepartment(){
    	view.getModeltableDepartment().setRowCount(0);
		for(Department dep : model.getDepartment()){
			String leaderStr = "";
			if(dep.getLeader() != null)
				leaderStr = dep.getLeader().getName() +" "+ dep.getLeader().getFirstName();
			if(dep.getName().contains(view.getTxtSearchDepartment().getText()) ||
			leaderStr.contains(view.getTxtSearchDepartment().getText()) ||
			String.valueOf(dep.getEmployees().size()).contains(view.getTxtSearchDepartment().getText())
			){
				view.getModeltableDepartment().addRow(new Object[]{
						dep.getName(),
						leaderStr,
						dep.getEmployees().size()
				});
			}
		}
	}
	
	/**
	 * searchPointing
	 */
	void searchPointing() { 
		view.getModeltablePointing().setRowCount(0);
		for(Day day : model.getDays()){
			if(day.getArrivingPointing()!=null){
				if(day.getEmployee().getName().contains(view.getTxtSearchPointing().getText()) ||
				day.getEmployee().getFirstName().contains(view.getTxtSearchPointing().getText()) ||
				day.getArrivingPointing().getDate().toString().contains(view.getTxtSearchPointing().getText()) ||
				model.getDepartmentEmployee(day.getEmployee()).getName().contains(view.getTxtSearchPointing().getText()) ||
				"Arriving".contains(view.getTxtSearchPointing().getText())
				){
					if(!view.getChckbxDaillyPointingsOnly().isSelected()||
						(view.getChckbxDaillyPointingsOnly().isSelected()&&
						day.getArrivingPointing().getDate().getLdt().getYear() == new Date().getLdt().getYear()&&
						day.getArrivingPointing().getDate().getLdt().getDayOfYear() == new Date().getLdt().getDayOfYear()))
						view.getModeltablePointing().addRow(new Object[]{
								day.getEmployee().getName(),
								day.getEmployee().getFirstName(),
								day.getArrivingPointing().getDate(),
								"Arriving " 
								+
								(day.getArrivingPointing().getDate().getLdt().getMinute()-day.getEmployee().getArrivingHour().getLdt().getMinute() + (day.getArrivingPointing().getDate().getLdt().getHour()-day.getEmployee().getArrivingHour().getLdt().getHour())*60)			
								+
								" min"
						});
				}
			}
			if(day.getLeavingPointing()!=null){
				if(day.getEmployee().getName().contains(view.getTxtSearchPointing().getText()) ||
				day.getEmployee().getFirstName().contains(view.getTxtSearchPointing().getText()) ||
				day.getArrivingPointing().getDate().toString().contains(view.getTxtSearchPointing().getText()) ||
				model.getDepartmentEmployee(day.getEmployee()).getName().contains(view.getTxtSearchPointing().getText()) ||
				"Leaving".contains(view.getTxtSearchPointing().getText())
				){
					if(!view.getChckbxDaillyPointingsOnly().isSelected()||
						(view.getChckbxDaillyPointingsOnly().isSelected()&&
						day.getArrivingPointing().getDate().getLdt().getYear() == new Date().getLdt().getYear()&&
						day.getArrivingPointing().getDate().getLdt().getDayOfYear() == new Date().getLdt().getDayOfYear()))
					view.getModeltablePointing().addRow(new Object[]{
							day.getEmployee().getName(),
							day.getEmployee().getFirstName(),
							day.getLeavingPointing().getDate(),
							"Leaving " 
							+
							(day.getLeavingPointing().getDate().getLdt().getMinute()-day.getEmployee().getLeavingHour().getLdt().getMinute() + (day.getLeavingPointing().getDate().getLdt().getHour()-day.getEmployee().getLeavingHour().getLdt().getHour())*60)			
							+
							" min"
					});
				}
			}
		}
	}
	
	/**
	 * getPointing
	 * @param mouseEvent
	 */
	void getPointing(MouseEvent mouseEvent){
		JTable table =(JTable) mouseEvent.getSource();
        if (mouseEvent.getClickCount() == 2 && table.getSelectedRow() != -1) {
        	for(Day day : model.getDays()){
        		if((
        			(
        				day.getArrivingPointing().getDate() == table.getValueAt(table.getSelectedRow(), 2) 
        				&&
        				day.getLeavingPointing() == null 
        			) ||
        			(
        				
        				day.getArrivingPointing().getDate() == table.getValueAt(table.getSelectedRow(), 2) 	 
        				||
        				(
	        				day.getLeavingPointing() != null 
	        				&&
	        				day.getLeavingPointing().getDate() == table.getValueAt(table.getSelectedRow(), 2))
        				)
        			) && 
        			day.getEmployee() == model.getEmployee(table.getValueAt(table.getSelectedRow(), 0).toString(), table.getValueAt(table.getSelectedRow(), 1).toString())
        			){
		        	UpdatePointingView view = new UpdatePointingView();
		        	UpdatePointingController c = new UpdatePointingController(model, view);
		        	
		        	String state = table.getValueAt(table.getSelectedRow(), 3).toString();
        			if(state.contains("Arriving"))
						c.initController(day.getArrivingPointing());
        			if(state.contains("Leaving"))
        				c.initController(day.getLeavingPointing());
    				
        			c.getView().getBtnUpdate().addActionListener(new ActionListener() { 
    				    public void actionPerformed(ActionEvent e) {  
    				    	if(state.contains("Arriving"))
    				    		c.updatePointing(day.getArrivingPointing());
    				    	if(state.contains("Leaving"))
    				    		c.updatePointing(day.getLeavingPointing());
    				    	model=c.getModel();
    				    	refreshTableData();
    				    }
    				});
    				c.getView().getBtnDelete().addActionListener(new ActionListener() { 
    				    public void actionPerformed(ActionEvent e) { 
    				    	c.deletePointing(day, state);
    				    	model=c.getModel();
    				    	refreshTableData();
    				    }
    				});
        		}
        	}		        	
        }
	}
	
	/**
	 * getDepartment
	 * @param mouseEvent
	 */
	void getDepartment(MouseEvent mouseEvent){
		 JTable table =(JTable) mouseEvent.getSource();
        if (mouseEvent.getClickCount() == 2 && table.getSelectedRow() != -1) {
     		if(model.departmentExist(table.getValueAt(table.getSelectedRow(), 0).toString())){
     			UpdateDepartmentView view = new UpdateDepartmentView();
     			UpdateDepartmentController c = new UpdateDepartmentController(model, view);
     			Department dep = model.getDepartment(table.getValueAt(table.getSelectedRow(), 0).toString());
 				c.initController(dep);
 				c.getView().getBtnUpdate().addActionListener(new ActionListener() { 
 				    public void actionPerformed(ActionEvent e) {  
 				    	c.updateDepartment(dep);
 				    	model=c.getModel();
 				    	refreshTableData();
 				    }
 				});
 				c.getView().getBtnDelete().addActionListener(new ActionListener() { 
 				    public void actionPerformed(ActionEvent e) {  
 				    	c.deleteDepartment(dep);
 				    	model=c.getModel();
 				    	refreshTableData();
 				    }
 				}); 
     		}
        }
	}
	
	/**
	 * getEmployee
	 * @param mouseEvent
	 */
	void getEmployee(MouseEvent mouseEvent){
		JTable table =(JTable) mouseEvent.getSource();
        if (mouseEvent.getClickCount() == 2 && table.getSelectedRow() != -1) {
    		if(model.getEmployee(table.getValueAt(table.getSelectedRow(), 0).toString(), table.getValueAt(table.getSelectedRow(), 1).toString())!=null){
    			UpdateEmployeeView view = new UpdateEmployeeView();
    			UpdateEmployeeController c = new UpdateEmployeeController(model, view);
    			
    			Employee emp = model.getEmployee(table.getValueAt(table.getSelectedRow(), 0).toString(), table.getValueAt(table.getSelectedRow(), 1).toString());
				c.initController(emp);
				c.getView().getBtnUpdate().addActionListener(new ActionListener() { 
				    public void actionPerformed(ActionEvent e) {  
				    	c.updateEmployee(emp);
				    	model=c.getModel();
				    	refreshTableData();
				    }
				});
				c.getView().getBtnDelete().addActionListener(new ActionListener() { 
				    public void actionPerformed(ActionEvent e) {  
				    	c.deleteEmployee(emp);
				    	model=c.getModel();
				    	refreshTableData();
				    }
				}); 
    		}
    	}
	}
	
	/**
	 * updateConfig
	 */
	void updateConfig(){
		if(view.getTxtIPAdress().getText().length()!=0)
			Config.setIpAdress(view.getTxtIPAdress().getText());
		if(view.getTxtPortEmployee().getText().length()!=0 && isInt(view.getTxtPortEmployee().getText()))
			Config.setPortServerEmployee(Integer.parseInt(view.getTxtPortEmployee().getText()));
		if(view.getTxtPortPointing().getText().length()!=0 && isInt(view.getTxtPortPointing().getText()))
			Config.setPortServerPointing(Integer.parseInt(view.getTxtPortPointing().getText()));
		if(view.getTxtAccidentThreshold().getText().length()!=0 && isInt(view.getTxtAccidentThreshold().getText()))
			Config.setAccidentThreshold(Integer.parseInt(view.getTxtAccidentThreshold().getText()));	
		refreshTableData();
	}
	
	/**
	 * isInt
	 * @param chaine
	 * @return boolean
	 */
	boolean isInt(String chaine) {
		try {
			Integer.parseInt(chaine);
		} catch (NumberFormatException e){
			return false;
		}
		return true;
	}
}
