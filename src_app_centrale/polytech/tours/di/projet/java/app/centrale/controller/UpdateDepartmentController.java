package polytech.tours.di.projet.java.app.centrale.controller;

import javax.swing.JOptionPane;

import polytech.tours.di.projet.java.app.centrale.model.company.Company;
import polytech.tours.di.projet.java.app.centrale.model.company.Department;
import polytech.tours.di.projet.java.app.centrale.model.company.Employee;
import polytech.tours.di.projet.java.app.centrale.model.company.Manager;
import polytech.tours.di.projet.java.app.centrale.view.UpdateDepartmentView;

/**
 * UpdateDepartmentController
 * @author Administrateur
 *
 */
/**
 * @author Damien Auvray
 *
 */
public class UpdateDepartmentController {

	private Company model;
	private UpdateDepartmentView view;

	/**
	 * UpdateDepartmentController
	 * @param m
	 * @param v
	 */
	public UpdateDepartmentController(Company m, UpdateDepartmentView v) {
		// TODO Auto-generated constructor stub
		model = m;
		view = v;
		initView();
	}
	
	/**
	 * initView
	 */
	public void initView(){
		view.getBtnClose().addActionListener(e -> view.dispose());
		view.getBtnDelete().addActionListener(e -> view.dispose());
	}
	
	/**
	 * initController
	 * @param dep
	 */
	public void initController(Department dep){
		view.getTxtNom().setText(dep.getName());
		view.getCbManager().addItem("");
		for(Employee e : model.getEmployees()){
			if(e.getClass()==Manager.class){
				if(e.getClass()==Manager.class && !model.getLeader().contains(e) && model.getDepartmentEmployee(e)==dep)
					view.getCbManager().addItem(e.getName() + " " + e.getFirstName());
			}
		}
		if(dep.getLeader()!=null){
			view.getCbManager().addItem(dep.getLeader().getName() + " " + dep.getLeader().getFirstName());
			view.getCbManager().setSelectedItem(dep.getLeader().getName() + " " + dep.getLeader().getFirstName());
		}
	}
	
	/**
	 * updateDepartment
	 * @param dep
	 */
	public void updateDepartment(Department dep){
		if(view.getTxtNom().getText().length()!=0){
			model.getDepartment(dep.getName()).setName(view.getTxtNom().getText());
			for(Employee e: model.getEmployees()){
				if(view.getCbManager().getSelectedItem().toString().contains(e.getName()) &&
					view.getCbManager().getSelectedItem().toString().contains(e.getFirstName()))
					model.getDepartment(view.getTxtNom().getText()).updateLeader((Manager) e);
				else if(view.getCbManager().getSelectedItem().toString().length() == 0)
					model.getDepartment(view.getTxtNom().getText()).updateLeader();
			}
			view.dispose();
		}
	}
	
	/**
	 * deleteDepartment
	 * @param dep
	 */
	public void deleteDepartment(Department dep){
		int reponse = JOptionPane.showConfirmDialog(view,
                "Do you really want to delete this department ?",
                "Confirm",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE);
		if(reponse == JOptionPane.YES_OPTION ){
			model.removeDepartment(dep);
			view.dispose();
		}
	}

	/**
	 * getModel
	 * @return
	 */
	public Company getModel() {
		return model;
	}
 
	
	/**
	 * setModel
	 * @param model
	 */
	public void setModel(Company model) {
		this.model = model;
	}
	
	/**
	 * UpdateDepartmentView
	 * @return view
	 */
	public UpdateDepartmentView getView() {
		return view;
	}
	
	/**
	 * setView
	 * @param view
	 */
	public void setView(UpdateDepartmentView view) {
		this.view = view;
	}

}
