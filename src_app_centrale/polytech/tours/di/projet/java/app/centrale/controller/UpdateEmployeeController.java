package polytech.tours.di.projet.java.app.centrale.controller;

import java.util.ArrayList;

import javax.swing.JOptionPane;

import polytech.tours.di.projet.java.app.centrale.model.company.Company;
import polytech.tours.di.projet.java.app.centrale.model.company.Employee;
import polytech.tours.di.projet.java.app.centrale.model.company.Manager;
import polytech.tours.di.projet.java.app.centrale.model.time.Date;
import polytech.tours.di.projet.java.app.centrale.model.time.Day;
import polytech.tours.di.projet.java.app.centrale.model.time.Pointing;
import polytech.tours.di.projet.java.app.centrale.view.UpdateEmployeeView;

/**
 * UpdateEmployeeController
 * @author Damien Auvray
 *
 */
public class UpdateEmployeeController {
	private Company model;
	private UpdateEmployeeView view;

	/**
	 * UpdateEmployeeController
	 * @param m
	 * @param v
	 */
	public UpdateEmployeeController(Company m, UpdateEmployeeView v) {
		// TODO Auto-generated constructor stub
		model = m;
		view = v;
		initView();
	}
	
	/**
	 * initView
	 */
	public void initView(){
		view.getBtnClose().addActionListener(e -> view.dispose());
		view.getBtnDelete().addActionListener(e -> view.dispose());
	}
	
	/**
	 * initController
	 * @param emp
	 */
	public void initController(Employee emp){
		view.getTxtNom().setText(emp.getName());
		view.getTxtPrenom().setText(emp.getFirstName());
		view.getTxtArrivingDate().setText(emp.getArrivingHour().toString());
		view.getTxtLeavingDate().setText(emp.getLeavingHour().toString());
		view.getTxtDepartment().setText(model.getDepartmentEmployee(emp).getName());
		if(emp.getClass()==Manager.class)
			view.getTxtEmail().setText(((Manager) emp).getMail());
	}
	
	/**
	 * updateEmployee
	 * @param emp
	 */
	public void updateEmployee(Employee emp){
		boolean isValid=true;
		Date arriving = new Date();
		Date leaving = new Date();
		if(view.getTxtNom().getText().length()!=0 &&
			view.getTxtPrenom().getText().length()!=0 &&
			view.getTxtDepartment().getText().length()!=0 &&
			view.getTxtArrivingDate().getText().length()!=0 &&
			view.getTxtLeavingDate().getText().length()!=0	){
			
			try{
				arriving = new Date(view.getTxtArrivingDate().getText()); 
				leaving = new Date(view.getTxtLeavingDate().getText());
			}catch(Exception e){
				isValid=false;
			}
			if(isValid){
				emp.setName(view.getTxtNom().getText());
				emp.setFirstName(view.getTxtPrenom().getText());
				emp.setArrivingHour(arriving);
				emp.setLeavingHour(leaving);
				
				ArrayList<Pointing> pointings = new ArrayList<Pointing>();
				for(Day d : model.getDays()){
					if(d.getEmployee().getName().equals(emp.getName()) && d.getEmployee().getFirstName().equals(emp.getFirstName())){
						if(d.getArrivingPointing()!=null)
							pointings.add(d.getArrivingPointing());
							//d.getArrivingPointing().setEmployee(model.getEmployee(emp.getName(), emp.getFirstName()));
						if(d.getLeavingPointing()!=null)
							pointings.add(d.getLeavingPointing());
							//d.getLeavingPointing().setEmployee(model.getEmployee(emp.getName(), emp.getFirstName()));
					}	
				}
				if(view.getTxtDepartment().getText().compareTo(model.getDepartmentEmployee(emp).getName())!=0){
					if(model.getLeader(view.getTxtNom().getText(), view.getTxtPrenom().getText())!=null){
						model.getDepartmentEmployee(emp).removeLeader();
					}
					model.getDepartmentEmployee(emp).removeEmployee(emp);
					if(!model.departmentExist(view.getTxtDepartment().getText()))
						model.addDepartment(view.getTxtDepartment().getText());
					model.getDepartment(view.getTxtDepartment().getText()).addEmp(emp);						
				}
				if(emp.getClass()==Manager.class){
					if(view.getTxtEmail().getText().length()!=0)
						((Manager) emp).setMail(view.getTxtEmail().getText());
					else{
						model.removeEmployee(emp);
						model.addEmployee(emp.getName(), emp.getFirstName(), emp.getArrivingHour(), emp.getLeavingHour(), view.getTxtDepartment().getText());
						model.getEmployee(emp.getName(), emp.getFirstName()).updateOvertime(emp.getOvertime());
					}
				}else{
					if(view.getTxtEmail().getText().length()!=0){
						model.removeEmployee(emp);
						model.addManager(emp.getName(), emp.getFirstName(), emp.getArrivingHour(), emp.getLeavingHour(), view.getTxtEmail().getText(), view.getTxtDepartment().getText());
						model.getEmployee(emp.getName(), emp.getFirstName()).updateOvertime(emp.getOvertime());
					}
				}
				if(pointings.size()!=0) {
					for(Pointing p : pointings) {
						p.setEmployee(model.getEmployee(emp.getName(), emp.getFirstName()));
					}
					model.addDay(pointings);
				}
				view.dispose();
			}
		}
	}
	
	/**
	 * deleteEmployee
	 * @param emp
	 */
	public void deleteEmployee(Employee emp){
		int reponse = JOptionPane.showConfirmDialog(view,
                "Do you really want to delete this employee ?",
                "Confirm",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE);
		if(reponse == JOptionPane.YES_OPTION ){
			model.removeEmployee(emp);
			view.dispose();
		}
	}

	/**
	 * getModel
	 * @return model
	 */
	public Company getModel() {
		return model;
	}

	/**
	 * setModel
	 * @param model
	 */
	public void setModel(Company model) {
		this.model = model;
	}
	
	/**
	 * UpdateEmployeeView
	 * @return view
	 */
	public UpdateEmployeeView getView() {
		return view;
	}
	
	/**
	 * setView
	 * @param view
	 */
	public void setView(UpdateEmployeeView view) {
		this.view = view;
	}
}
