package polytech.tours.di.projet.java.app.centrale.controller;

import java.util.ArrayList;

import javax.swing.JOptionPane;

import polytech.tours.di.projet.java.app.centrale.model.company.*;
import polytech.tours.di.projet.java.app.centrale.model.time.Date;
import polytech.tours.di.projet.java.app.centrale.model.time.Day;
import polytech.tours.di.projet.java.app.centrale.model.time.Pointing;
import polytech.tours.di.projet.java.app.centrale.view.UpdatePointingView;

/**
 * UpdatePointingController
 * @author Damien Auvray
 *
 */
public class UpdatePointingController {
	private Company model;
	private UpdatePointingView view;

	/**
	 * UpdatePointingController
	 * @param m
	 * @param v
	 */
	public UpdatePointingController(Company m, UpdatePointingView v) {
		// TODO Auto-generated constructor stub
		model = m;
		view = v;
		initView();
	}
	
	/**
	 * initView
	 */
	public void initView(){
		view.getBtnClose().addActionListener(e -> view.dispose());
		view.getBtnDelete().addActionListener(e -> view.dispose());
	}
	
	/**
	 * initController
	 * @param pointing
	 */
	public void initController(Pointing pointing){
		view.getCbEmployee().addItem(pointing.getEmployee().getName() + " " + pointing.getEmployee().getFirstName());
		view.getTxtDate().setText(pointing.getDate().toString());
	}
	
	/**
	 * updatePointing
	 * @param pointing
	 */
	public void updatePointing(Pointing pointing) {
		// TODO Auto-generated method stub
		boolean isValid = true;
		Date date = new Date();
		try{
			date = new Date(view.getTxtDate().getText()); 
		}catch(Exception e){
			isValid = false;
		}
		if(isValid){
			pointing.setDate(date);
			model.addPointing(pointing);
			view.dispose();
		}
	}
	
	/**
	 * deletePointing
	 * @param day
	 * @param state
	 */
	public void deletePointing(Day day, String state){
		int reponse = JOptionPane.showConfirmDialog(view,
                "Do you really want to delete this pointing ?",
                "Confirm",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE);
		if(reponse == JOptionPane.YES_OPTION ){
			ArrayList<Pointing> pointings = new ArrayList<>();
			if(state.contains("Arriving")  && day.getLeavingPointing()!=null)
				pointings.add(day.getLeavingPointing());
			if(state.contains("Leaving"))
				pointings.add(day.getArrivingPointing());
			model.getDays().remove(day);
			for(Day d: model.getDays()) {
				if(d.getEmployee().equals(day.getEmployee())) {
					if(d.getArrivingPointing()!=null)
						pointings.add(d.getArrivingPointing());
					if(d.getLeavingPointing()!=null)
						pointings.add(d.getLeavingPointing());
				}		
			}
			if(pointings.size()!=0)
				model.addDay(pointings);
			else
				day.getEmployee().refreshOvertime();
			view.dispose();
		}
	}	
	
	/**
	 * getModel
	 * @return
	 */
	public Company getModel() {
		return model;
	}
	
	/*
	 * setModel
	 */
	public void setModel(Company model) {
		this.model = model;
	}
	
	/**
	 * UpdatePointingView
	 * @return view
	 */
	public UpdatePointingView getView() {
		return view;
	}
	
	/**
	 * setView
	 * @param view
	 */
	public void setView(UpdatePointingView view) {
		this.view = view;
	}
	
	
}
