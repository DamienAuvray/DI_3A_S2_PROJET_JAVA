package polytech.tours.di.projet.java.app.centrale.model.company;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


//import polytech.tours.di.projet.java.app.centrale.model.tcp.Config;
import polytech.tours.di.projet.java.app.centrale.model.time.Date;
import polytech.tours.di.projet.java.app.centrale.model.time.Day;
import polytech.tours.di.projet.java.app.centrale.model.time.Pointing;
 
/**
 * Company
 * @author Damien Auvray
 *
 */
@SuppressWarnings("serial")
public class Company implements Serializable{
 
    /** Associations */
    private ArrayList<Day> days = new ArrayList<Day>();
    private ArrayList<Department> departments =  new ArrayList<Department>();
    //private Config config = new Config();
    
    /**
     * import an employees list from a .CSV
     * @param fileName : the name of the file 
     * @throws IOException 
     */
    public void importCSV ( File fileName ) throws IOException
    {
        BufferedReader br;
        String[] buffer;
        Date arriving;
        Date leaving;
        br = new BufferedReader(new FileReader(fileName));

        String line;
        
        while ((line = br.readLine()) != null) {
            buffer = line.split(";");
            if(getEmployee(buffer[0], buffer[1])==null) {
            arriving = new Date(buffer[2]);
            leaving = new Date(buffer[3]);
	            if(getDepartment(buffer[6])==null){
	            	addDepartment(buffer[6]);
	            }
	            if(buffer[5].equals("null")){
	            	addEmployee(buffer[0], buffer[1], arriving, leaving, buffer[6]);
	            }
	            else{
		            if(buffer[7].equals("Leader"))
		            	getDepartment(buffer[6]).setLeader(new Manager(buffer[0], buffer[1], arriving, leaving, buffer[5]));
		            else
		            	addManager(buffer[0], buffer[1], arriving, leaving, buffer[5], buffer[6]);
	            }
	            getEmployee(buffer[0], buffer[1]).updateOvertime(Long.parseLong(buffer[4]));
            }
        }
 
        br.close();
       
    }
    
    /**
     * export the employees list to a .CSV
     * @param fileName the name of the file that we create
     * @throws IOException 
     */
    public void exportCSV ( File fileName ) throws IOException
    {
    	FileWriter writer = new FileWriter(fileName);
    	
        //default customQuote is empty
		StringBuilder sb = new StringBuilder();
    	for(Employee emp : this.getEmployees()){
    		boolean first = true;
        	List<String> values =new ArrayList<String>();
    		values.add(emp.getName());
    		values.add(emp.getFirstName());
    		values.add(String.valueOf(emp.getArrivingHour()));
    		values.add(String.valueOf(emp.getLeavingHour()));
    		values.add(String.valueOf(emp.getOvertime()));
    		if(emp.getClass()==Manager.class)
    			values.add(String.valueOf(((Manager) emp).getMail()));
    		else
    			values.add(null);
    		values.add(String.valueOf(getDepartmentEmployee(emp).getName()));
    		if(getLeader(emp.getName(), emp.getFirstName())!=null)
    			values.add("Leader");
    		else
    			values.add("Employee");

            for (String value : values) {
                if (!first) {
                    sb.append(";");
                }
                sb.append(value);
                first = false;
            }
            sb.append(System.getProperty("line.separator"));
    	}
	    writer.write(sb.toString());
        writer.flush();
        writer.close();
   
    }
    

	/**
	 * pointingCSV
	 * @param fileName
	 * @throws IOException
	 */
	public void pointingCSV(File fileName) throws IOException {
		BufferedReader br;
        String[] buffer;
        ArrayList<Pointing> pointings = new ArrayList<Pointing>();
        br = new BufferedReader(new FileReader(fileName));
        String line;
        while ((line = br.readLine()) != null) {
            buffer = line.split(";");
            if(getEmployee(buffer[0], buffer[1])!=null)
            	pointings.add(new Pointing(getEmployee(buffer[0], buffer[1]), new Date(buffer[2])));
        }
        addDay(pointings);
        br.close();
	}
    
	/**
	 * stub
	 */
    public void stub() {
    	try {
			importCSV(new File("EmployeesFiles/importCSV.csv"));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		addEmployee("Marlin", "Alexis",new Date("2004-02-10 08:00"), new Date("2004-02-10 19:00"), "accounting");
		addManager("Datee", "Nicolas", new Date("2004-02-10 08:00"), new Date("2004-02-10 19:00"), "loic.gervois@etu.univ-tours.fr", "accounting");
		addDepartment("technology");
		addDepartment("finance");
		ArrayList<Pointing> pointings = new ArrayList<Pointing>();
		/**
		 * Here a employee failed and point 3 time.
		 * Donovan: +30min overtime.
		 */
		pointings.add(new Pointing(getEmployee("Guillot", "Donovan"), new Date("2004-03-02 08:00")));
		pointings.add(new Pointing(getEmployee("Guillot", "Donovan"), new Date("2004-03-02 17:00")));
		pointings.add(new Pointing(getEmployee("Guillot", "Donovan"), new Date("2004-03-02 18:30")));
		/**
		 * Damien: -30min overtime.
		 */
		pointings.add(new Pointing(getEmployee("Auvray", "Damien"), new Date("2004-03-02 08:00")));
		pointings.add(new Pointing(getEmployee("Auvray", "Damien"), new Date("2004-03-02 18:00")));
		/**
		 * Pointing mixed.
		 */
		pointings.add(new Pointing(getEmployee("Guillot", "Donovan"), new Date("2004-03-03 18:00")));
		pointings.add(new Pointing(getEmployee("Guillot", "Donovan"), new Date("2004-03-03 08:00")));
		/**
		 * Pointing Manager
		 */
		pointings.add(new Pointing(getLeader("Leroy", "Valentin"), new Date("2004-03-03 08:00")));
		pointings.add(new Pointing(getLeader("Leroy", "Valentin"), new Date("2004-03-03 18:00")));
		
		addDay(pointings);
		
		//getDepartment("accounting").removeLeader();
    }
    
    /* DEPARTMENTS */

	/**
	 * setDepartment
	 * @param List of department
	 */
	public void setDepartment(ArrayList<Department> department) {
		this.departments = department;
	}
    
	/**
	 * getDepartment
	 * @return A list of department
	 */
	public ArrayList<Department> getDepartment() {
		return departments;
	}	
	
	/**
	 * getDepartment
	 * @param dapartmentName
	 * @return Department or null
	 */
	public Department getDepartment(String dapartmentName){
		for(Department d : departments){
			if((d.getName()).equals(dapartmentName))
				return d;
		}
		return null;
	}
	
	/**
	 * getDepartmentEmployee
	 * @param String name
	 * @param String firstName
	 * @return Department or null
	 */
	public Department getDepartmentEmployee(String name, String firstName){
		for(Department d : departments){
			if((d.getEmployee(name, firstName)).equals(getEmployee(name, firstName)))
				return d;
		}
		return null;
	}
	
	/**
	 * getDepartmentEmployee
	 * @param Employee employee
	 * @return Department or null
	 */
	public Department getDepartmentEmployee(Employee emp){
		for(Department d : departments){
			for(Employee e : d.getEmployees()){
				if(e.equals(emp))
					return d;
			}
		}
		return null;
	}

	/**
	 * departmentExist
	 * @param String dapartmentName
	 * @return Boolean true or false
	 */
	public boolean departmentExist(String dapartmentName){
		for(Department d : departments){
			if((d.getName()).equals(dapartmentName))
				return true;
		}
		return false;
	}
	
	/**
	 * addDepartment
	 * Create a new Department if not exist
	 * @param String depName
	 */
	public void addDepartment( String depName ){
    	if(!departmentExist(depName))
    		getDepartment().add(new Department(depName));
    }
	
	/**
	 * removeDepartment
	 * Delete a department and everything that are inside
	 * @param Department department
	 */
	public void removeDepartment(Department department){
		if(departmentExist(department.getName())){
			getDepartment(department.getName()).removeLeader();
			getDepartment(department.getName()).removeAllEmployee();
			departments.remove(getDepartment(department.getName()));
		}	
	}
	

	/* EMPLOYEES */
	
	/**
	 * getEmployees
	 * @return List of Employees
	 */
	public ArrayList<Employee> getEmployees() {
		ArrayList<Employee> listEmployees = new ArrayList<Employee>();
		for(Department d : departments){
			for(Employee e : d.getEmployees()){
				listEmployees.add(e);
			}
		}
		return listEmployees;
	}
	
	/**
	 * getEmployee
	 * @param name
	 * @param firstName
	 * @return Employee or null
	 */
	public Employee getEmployee(String name, String firstName){;
		for(Department d : departments){
			if(d.getEmployee(name, firstName) != null){
				return d.getEmployee(name, firstName);
			}
		}
		return null;
	}
	
	/**
	 * removeEmployee
	 * @param Employee emp
	 */
	public void removeEmployee(Employee emp){
		for(Department dep : getDepartment()){
			if(dep.getLeader() != null)
				if(dep.getLeader().equals(emp))
					dep.removeLeader();
		}
		getDepartmentEmployee(emp).removeEmployee(emp);
		ArrayList<Day>listToRemove=new ArrayList<Day>();
		for(Day d : getDays()){
			if(d.getEmployee().getName().equals(emp.getName()) && d.getEmployee().getFirstName().equals(emp.getFirstName())){
				listToRemove.add(d);
			}		
		}
		getDays().removeAll(listToRemove);
	}
       
	/**
	 * addEmployee
	 * @param name
	 * @param firstName
	 * @param arrivingHour
	 * @param leavingHour
	 * @param depName
	 */
    public void addEmployee(String name, String firstName, Date arrivingHour, Date leavingHour, String depName){
    	boolean bool = false;
    	if(!departmentExist(depName))
    		getDepartment().add(new Department(depName));
    	for(Department d :getDepartment()){
    		if(bool!=true)
    			bool=d.employeeExist(name, firstName);
    	}
    	if(!bool)
    		getDepartment(depName).addEmp(name, firstName, arrivingHour, leavingHour);
    }
    
    /**
	 * addManager
	 * @param name
	 * @param firstName
	 * @param arrivingHour
	 * @param leavingHour
	 * @param depName
	 */
    public void addManager(String name, String firstName, Date arrivingHour, Date leavingHour, String email, String depName){
    	boolean bool = false;
    	if(!departmentExist(depName))
    		getDepartment().add(new Department(depName));
    	for(Department d :getDepartment()){
    		if(bool!=true)
    			bool=d.employeeExist(name, firstName);
    	}
    	if(!bool)
    		getDepartment(depName).addManager(name, firstName, arrivingHour, leavingHour, email);
    }
    
    /**
     * addEmployee
     * @param emp
     * @param depName
     */
    public void addEmployee(Employee emp, String depName){
    	boolean bool = false;
    	if(!departmentExist(depName))
    		getDepartment().add(new Department(depName));
    	for(Department d :getDepartment()){
    		if(bool!=true)
    			bool=d.employeeExist(emp);
    	}
    	if(!bool)
    		getDepartment(depName).addEmp(emp);
    }
    
    /* LEADER */
    
    /**
    * getLeader
    * @param name
    * @param firstname
    * @return Manager or null
    */
    public Manager getLeader(String name, String firstname){
		for(Department d : departments){
			if(d.getLeader()!=null){
				if(d.getLeader().getName().equals(name) && d.getLeader().getFirstName().equals(firstname))
					return d.getLeader();
				}
		}
		return null;
	}
    
    /**
     * getLeader
     * @return List of managers
     */
    public ArrayList<Manager> getLeader(){
    	ArrayList<Manager> listManagers = new ArrayList<Manager>();
		for(Department d : departments){
			if(d.getLeader()!=null)
				listManagers.add(d.getLeader());
		}
		return listManagers;
	}
    
    
    /* DAYS */
	
    /**
     * getDays
     * @return List of days
     */
    public ArrayList<Day> getDays() {
		return days;
	}
    
    /**
     * setDays
     * @param days
     */
	public void setDays(ArrayList<Day> days) {
		this.days = days;
	}
	
	/**
	 * getDays
	 * @param day
	 * @return Boolean true or false
	 */
	public boolean dayExist(Day day){
		for(Day d : days){
			if(d.getDate().getLdt().getDayOfYear()==day.getDate().getLdt().getDayOfYear() && d.getEmployee()==day.getEmployee())
				return true;
		}
		return false;
	}
	
	/**
	 * addDay
	 * Sort and add all the days from a list of pointing
	 * also update overtime for each day
	 * @param List of pointing
	 */
	public void addDay(ArrayList<Pointing> pointings) {
		for(Employee emp: getEmployees()){
			emp.refreshOvertime();
		}
		for(Day d: days) {
			if(d.getArrivingPointing()!=null)
				pointings.add(d.getArrivingPointing());
			if(d.getLeavingPointing()!=null)
				pointings.add(d.getLeavingPointing());
		}
		days.clear();

		ArrayList<Pointing> pts = new ArrayList<Pointing>();
		for(Pointing pointingLoop1: pointings) {
			pts.add(new Pointing(pointingLoop1));
			for(Pointing pointingLoop2: pointings) {
				if(pointingLoop1.getDate().getLdt().getDayOfYear()==pointingLoop2.getDate().getLdt().getDayOfYear() 
						&& pointingLoop1.getDate().getLdt().getYear()==pointingLoop2.getDate().getLdt().getYear() 
						&& pointingLoop1!=pointingLoop2 
						&& pointingLoop1.getEmployee()==pointingLoop2.getEmployee()
						) {
					pts.add(new Pointing(pointingLoop2));
				}
			}
			if(!dayExist(new Day(new ArrayList<Pointing>(pts)))) {
				days.add(new Day(new ArrayList<Pointing>(pts)));
				new Day(new ArrayList<Pointing>(pts)).updateOvertime();
			}
			pts.clear();
		}
	}
	
	/* Pointing */
	
	public void addPointing(Pointing pointing){
		ArrayList<Pointing> pts = new ArrayList<Pointing>();
		pts.add(pointing);
		addDay(pts);
	}
	
	/* To String */
	
	/**
	 * toString
	 */
    @Override
	public String toString(){
		String retour = "EMPLOYEES : \n\n";
		for(Employee e : getEmployees())
			retour += e+"\n";
		retour += "\n\nDEPARTMENTS : \n\n";
		for(Department d : departments)
			retour += d+"\n";
		retour += "\n\nHISTORIC : \n\n";
		for(Day d : days)
			retour += d+"\n";
		return retour;
	}
}
