package polytech.tours.di.projet.java.app.centrale.model.company;

import java.io.Serializable;
import java.util.ArrayList;

import polytech.tours.di.projet.java.app.centrale.model.time.Date;

/**
 * Department
 * @author Damien Auvray
 *
 */
@SuppressWarnings("serial")
public class Department implements Serializable{
    /** Attributes */
    private String name;
    /** Associations */
    private ArrayList<Employee> employees = new ArrayList<Employee>();
    private Manager leader;

    /**
     * Department constructor
     * @param name
     */
    public Department(String name){
    	this.name = name;
    }
    
    /**
     * getName
     * @return String name
     */
	public String getName() {
		return name;
	}
	
	/**
	 * setName
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}	
	
	 /**
     * getLeader
     * @return Manager manager
     */
	public Manager getLeader() {
		return leader;
	}

	/**
	 * setLeader
	 * add new manager only if leader is null
	 * @param leader
	 */
	public void setLeader(Manager leader) {
		if(this.leader==null){
			this.leader = leader;
			addEmp(leader);
		}
	}
	
	/**
	 * updateLeader
	 * update new manager
	 * @param leader
	 */
	public void updateLeader(Manager leader) {
		this.leader = leader;
	}
	
	/**
	 * updateLeader
	 * update as null
	 */
	public void updateLeader() {
		this.leader = null;
	}

	/**
	 * getEmployee
	 * @param name
	 * @param firstname
	 * @return Employee or null
	 */
	public Employee getEmployee(String name, String firstname){
		for(Employee e: employees){
			if(e.getName().equals(name) && e.getFirstName().equals(firstname))
				return e;
		}
		return null;
	}

	/**
	 * getEmployees
	 * @return List of employees
	 */
	public ArrayList<Employee> getEmployees() {
		return employees;
	}
	
	/**
	 * removeLeader
	 */
	public void removeLeader(){
		leader = null;
	}
	
	/**
	 * removeAllEmployee
	 */
	public void removeAllEmployee(){
		employees.removeAll(employees);
	}
	
	/**
	 * employeeExist
	 * @param employee
	 * @return Boolean true or false
	 */
	public boolean employeeExist(Employee employee){
		for(Employee e : employees){
			if(e.equals(employee))
				return true;
		}
		return false;
	}
	
	/**
	 * employeeExist
	 * @param name
	 * @param firstName
	 * @return Boolean true or false
	 */
	public boolean employeeExist(String name, String firstName){
		for(Employee e : employees){
			if(e.getName().equals(name) && e.getFirstName().equals(firstName))
				return true;
		}
		return false;
	}
	
	/**
	 * removeEmployee
	 * @param employee
	 */
	public void removeEmployee(Employee employee){
		if(employeeExist(employee)){
			employees.remove(getEmployee(employee.getName(), employee.getFirstName()));
		}
	}
	
	/**
	 * addEmp
	 * @param name
	 * @param firstName
	 * @param arrivingHour
	 * @param leavingHour
	 */
	public void addEmp(String name, String firstName, Date arrivingHour, Date leavingHour) {
		Employee newEmp = new Employee(name, firstName, arrivingHour, leavingHour);
		employees.add(newEmp);
	}
	public void addManager(String name, String firstName, Date arrivingHour, Date leavingHour, String email) {
		Manager newEmp = new Manager(name, firstName, arrivingHour, leavingHour, email);
		employees.add(newEmp);
	}
	public void addEmp(Employee emp) {
		employees.add(emp);
	}
	
	/**
	 * equals
	 * @param dep
	 * @return true if are equals. Else false.
	 */
	public boolean equals(Department dep){
		   return (this.name.equals(dep.getName()));
	}
	
	/**
	 * toString
	 */
	public String toString(){
		String retour = "";
		retour += name+"\n";
		if(leader!=null)
			retour += "Manager : " +leader.getName() + " " + leader.getFirstName() +"\n";
		return retour;
	}
	

 
}
