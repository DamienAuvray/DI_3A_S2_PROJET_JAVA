package polytech.tours.di.projet.java.app.centrale.model.company;

import java.io.Serializable;

import polytech.tours.di.projet.java.app.centrale.model.time.Date;

/**
 * Employee
 * @author Damien Auvray
 *
 */
@SuppressWarnings("serial")
public class Employee implements Serializable{
    
	/** Attributes */
    private String name;
    private String firstName;
    private Date arrivingHour;
    private Date leavingHour;
    private int overtime;

    /**
     * Employee constructor
     * @param nameP
     * @param firstNameP
     * @param arriving
     * @param leaving
     */
    public Employee(String nameP, String firstNameP, Date arriving, Date leaving ){
    	name = nameP;
    	firstName = firstNameP;
    	arrivingHour = arriving;
    	leavingHour = leaving;
    	overtime = 0;
    }
    	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	/**
	 * @return the overtime
	 */
	public int getOvertime() {
		return overtime;
	}
	
	/**
	 * @param l the overtime to set
	 */
	public void updateOvertime(long l) {
		this.overtime += l;
	}
	
	/**
	 * reset overtime to 0
	 */
	public void refreshOvertime(){
		overtime = 0;
	}
	
	/**
	 * @return the arrivingHour
	 */
	public Date getArrivingHour() {
		Date d = new Date();
		arrivingHour = new Date(d.getLdt().getYear(), d.getLdt().getMonthValue(), d.getLdt().getDayOfMonth(), arrivingHour.getLdt().getHour(), arrivingHour.getLdt().getMinute());
		return arrivingHour;
	}
	
	/**
	 * @param arrivingHour the arrivingHour to set
	 */
	public void setArrivingHour(Date arrivingHour) {
		Date d = new Date();
		leavingHour = new Date(d.getLdt().getYear(), d.getLdt().getMonthValue(), d.getLdt().getDayOfMonth(), leavingHour.getLdt().getHour(), leavingHour.getLdt().getMinute());
		this.arrivingHour = arrivingHour;
	}
	
	/**
	 * @return the leavingHour
	 */
	public Date getLeavingHour() {
		Date d = new Date();
		leavingHour = new Date(d.getLdt().getYear(), d.getLdt().getMonthValue(), d.getLdt().getDayOfMonth(), leavingHour.getLdt().getHour(), leavingHour.getLdt().getMinute());
		return leavingHour;
	}
	
	/**
	 * @param leavingHour the leavingHour to set
	 */
	public void setLeavingHour(Date leavingHour) {
		Date d = new Date();
		leavingHour = new Date(d.getLdt().getYear(), d.getLdt().getMonthValue(), d.getLdt().getDayOfMonth(), leavingHour.getLdt().getHour(), leavingHour.getLdt().getMinute());
		this.leavingHour = leavingHour;
	}
	
	/**
	 * @param Object employee
	 * @return true if are equals. Else false.
	 */
	public boolean equals(Employee emp){
		return (this.name.equals(emp.getName()) && this.firstName.equals(emp.getFirstName()));
	}
	
	/**
	 * To string method.
	 */
	public String toString(){
		return name+" "+firstName+" \narriving : "+arrivingHour+" \nleaving : "+leavingHour+" \novertime : "+overtime+"\n";
	}
    
}

