package polytech.tours.di.projet.java.app.centrale.model.company;

import java.io.Serializable;

import polytech.tours.di.projet.java.app.centrale.model.time.Date;

/**
 * Manager
 * @author Damien Auvray
 *
 */
@SuppressWarnings("serial")
public class Manager extends Employee implements Serializable{
    /** Attributes */
    private String email;

    /**
     * Manager constructor
     * @param nameP
     * @param firstNameP
     * @param arriving
     * @param leaving
     * @param email
     */
    public Manager(String nameP, String firstNameP, Date arriving, Date leaving, String email){
    	super(nameP, firstNameP, arriving, leaving);
    	this.email = email;
    }
    
	/**
	 * @return the eMail
	 */
	public String getMail() {
		return email;
	}

	/**
	 * @param eMail the eMail to set
	 */
	public void setMail(String eMail) {
		this.email = eMail;
	}

	/**
	 * toString
	 */
	public String toString(){
		return super.toString() + "mail : "+getMail()+"\n "; 
	}
} 
