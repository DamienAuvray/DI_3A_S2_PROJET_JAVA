package polytech.tours.di.projet.java.app.centrale.model.config;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Config
 * @author Damien Auvray
 *
 */
public class Config{
	private static String ipAdress="localhost";
	private static int portServerEmployee=8081;
	private static int portServerPointing=8085;
	private static int accidentThreshold=0;
	
	/**
	 * getIpAdress
	 * @return ipAdress
	 */
	public static String getIpAdress() {
		return ipAdress;
	}
	
	/**
	 * setIpAdress
	 * @param ipAdress
	 */
	public static void setIpAdress(String ipAdress) {
		Config.ipAdress = ipAdress;
	}
	
	/**
	 * getPortServerEmployee
	 * @return portServerEmployee
	 */
	public static int getPortServerEmployee() {
		return portServerEmployee;
	}
	
	/**
	 * setPortServerEmployee
	 * @param portServerEmployee
	 */
	public static void setPortServerEmployee(int portServerEmployee) {
		Config.portServerEmployee = portServerEmployee;
	}
	
	/**
	 * getPortServerPointing
	 * @return portServerPointing
	 */
	public static int getPortServerPointing() {
		return portServerPointing;
	}
	
	/**
	 * setPortServerPointing
	 * @param portServerPointing
	 */
	public static void setPortServerPointing(int portServerPointing) {
		Config.portServerPointing = portServerPointing;
	}
	
	/**
	 * getAccidentThreshold
	 * @return accidentThreshold
	 */
	public static int getAccidentThreshold() {
		return accidentThreshold;
	}
	
	/**
	 * setAccidentThreshold
	 * @param accidentThreshold
	 */
	public static void setAccidentThreshold(int accidentThreshold) {
		Config.accidentThreshold = accidentThreshold;
	}
	
	/**
	 * save
	 */
	public static void save() {
		Properties prop = new Properties();
		 
    	try {
    		//set the properties value
    		prop.setProperty("ipAdress", ipAdress);
    		prop.setProperty("portServerEmployee",  String.valueOf(portServerEmployee));
    		prop.setProperty("portServerPointing",  String.valueOf(portServerPointing));
    		prop.setProperty("accidentThreshold",  String.valueOf(accidentThreshold));
 
    		//save properties to project root folder
    		prop.store(new FileOutputStream(".config"), null);
 
    	} catch (IOException ex) {
    		ex.printStackTrace();
        }
	}
	
	/**
	 * update
	 */
	public static void update() {
		try {
			Properties prop = new Properties();
			InputStream input = new FileInputStream(".config");
			// load a properties file
			prop.load(input);
			ipAdress = prop.getProperty("ipAdress");
			portServerEmployee = Integer.parseInt(prop.getProperty("portServerEmployee"));
			portServerPointing = Integer.parseInt(prop.getProperty("portServerPointing"));
			accidentThreshold = Integer.parseInt(prop.getProperty("accidentThreshold"));
			input.close();
		} catch (Exception e) {
		}
	}
	
}
