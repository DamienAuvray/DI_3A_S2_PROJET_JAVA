package polytech.tours.di.projet.java.app.centrale.model.tcp;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.Callable;

import polytech.tours.di.projet.java.app.centrale.model.time.Pointing;

/**
 * ServerPointing
 * @author Damien Auvray
 *
 */
public class ServerPointing implements Callable<ArrayList<Pointing>>{
	/** the TCP socket */
	Socket s;
	/** the reading stream */
	ObjectInputStream ois;
	/** the pointing read */
	ArrayList<Pointing> pointings = new ArrayList<Pointing>();
	
	/**
	 * the constructor 
	 * @param s
	 */
	public ServerPointing(Socket s) {
		this.s = s;
	}

	/**
	 * the call method of the callable interface
	 * read a pointing from the reading stream 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<Pointing> call(){
		try {
			System.out.println("ServerPointing reading...");
			
			ois = new ObjectInputStream(s.getInputStream());
			pointings = (ArrayList<Pointing>) ois.readObject();
			
			System.out.println("ServerPointing reading done ...");
			
			ois.close();
			s.close();
		} catch (ClassNotFoundException | IOException e) {
		}

		return pointings;
	}
}
