package polytech.tours.di.projet.java.app.centrale.model.tcp;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import polytech.tours.di.projet.java.app.centrale.model.company.Company;
import polytech.tours.di.projet.java.app.centrale.model.config.Config;

/**
 * TCPServerEmployee
 * @author Damien Auvray
 *
 */
public class TCPServerEmployee implements Runnable{
	/** declare sockets */
	InetSocketAddress isA;
	ServerSocket ss;
	/** declare the employee's list */
	Company company;
	

	/**
	 * the constructor
	 * @param employees
	 */
	public TCPServerEmployee(Company company){
		this.company = company;
	}
	
	/**
	 * the run method of the runnable interface 
	 * make some connections and run some background threads that monitor the stream
	 */
	@Override
	public void run() {
		isA = new InetSocketAddress(Config.getIpAdress(),Config.getPortServerEmployee());  
		try {
			ss = new ServerSocket(isA.getPort());
			System.out.println("TCPServerEmployee Launched");
		} catch (IOException e) {
			System.exit(0);
			e.printStackTrace();
		} 

        while(true)
			try {
				new Thread(new ServerEmployee(ss.accept(), company.getEmployees())).start();
			} catch (IOException e) {
				e.printStackTrace();
			}		
	}

}
