package polytech.tours.di.projet.java.app.centrale.model.tcp;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import polytech.tours.di.projet.java.app.centrale.model.config.Config;
import polytech.tours.di.projet.java.app.centrale.model.time.Pointing;

/**
 * TCPServerPointing
 * @author Damien Auvray
 *
 */
public class TCPServerPointing implements Runnable{
	
	/** attributes */
	/** declare sockets */
	private ServerSocket ss; 
	private InetSocketAddress isA;
	/** declare pointing */
	private ArrayList<Pointing> pointings = new ArrayList<Pointing>();
	
	/**
	 * the run method of the runnable interface
	 * waiting for some pointings receive by the callable task
	 */
	@Override
	public void run() {		
		ExecutorService executor = Executors.newFixedThreadPool(1);	
		isA = new InetSocketAddress(Config.getIpAdress(), Config.getPortServerPointing());  

		try {
			ss = new ServerSocket(isA.getPort());
			System.out.println("TCPServerPoiting Launched");
		} catch (IOException e) {
			System.exit(0);
			e.printStackTrace();
		}

		
		while(true){
			try {
				try {
					pointings.addAll(executor.submit(new ServerPointing(ss.accept())).get());
				} catch (ExecutionException | IOException e) {
					e.printStackTrace();
				}
			} catch (InterruptedException e2) {
				e2.printStackTrace();
			}
		}
				
	}
	
	/**
	 * @return the pointing
	 */
	public ArrayList<Pointing> getPointing() {
		return pointings;
	}
	/**
	 * @param pointing the pointing to set
	 */
	public void setPointing(ArrayList<Pointing> pointings) {
		this.pointings = pointings;
	}

	/**
	 * @return the ss
	 */
	public ServerSocket getSs() {
		return ss;
	}
	/**
	 * @param ss the ss to set
	 */
	public void setSs(ServerSocket ss) {
		this.ss = ss;
	}


}