package polytech.tours.di.projet.java.app.centrale.model.time;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

/**
 * Date
 * @author Damien Auvray
 *
 */
@SuppressWarnings("serial")
public class Date implements Serializable{
        
        /** Attributes */
    private LocalDateTime ldt;

    /**
     * Date constructor
     */
    public Date() {
            ldt = LocalDateTime.now();
            this.roundedTime();
        }
    
    /**
     * Date constructor
     * @param YYYY
     * @param MM
     * @param DD
     * @param hh
     * @param mm
     */
    public Date(int YYYY, int MM, int DD, int hh, int mm) {
            ldt = LocalDateTime.of(YYYY, MM, DD, hh, mm);
            this.roundedTime();
        }
    
    /**
     * Date constructor
     * @param str
     */
    public Date(String str) {
    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
    	ldt = LocalDateTime.parse(str, formatter);
	    this.roundedTime();
	}
    
    /**
     * toString
     */
    @Override
        public String toString() {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
                return ldt.format(formatter);
        }

     /**
     * rounded up to the nearest fifteen minute increment
     * @return LocalTime : the hour rounded
     */
    public LocalDateTime roundedTime (  ){
	    ldt = ldt.truncatedTo(ChronoUnit.MINUTES);
	    if(ldt.getMinute()%15<=7)
	        ldt = ldt.minusMinutes(ldt.getMinute()%15);
	    else
	        ldt = ldt.plusMinutes(15-ldt.getMinute()%15);
		return ldt;
        
    }
    
    /**
     * getLdt
     * @return LocalDateTime
     */
    public LocalDateTime getLdt() {
            return ldt;
    }
    
    /**
     * setLdt
     * @param ldt
     */
    public void setLdt(LocalDateTime ldt) {
            this.ldt = ldt;
            this.roundedTime();
    }
}

// ## Implementation preserve start class closing. 
// ## Implementation preserve end class closing. 

