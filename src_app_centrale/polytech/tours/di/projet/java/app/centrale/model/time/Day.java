package polytech.tours.di.projet.java.app.centrale.model.time;

import java.io.Serializable;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import polytech.tours.di.projet.java.app.centrale.model.company.Employee;
import polytech.tours.di.projet.java.app.centrale.model.config.Config;

/**
 * Day
 * @author Damien Auvray
 *
 */
@SuppressWarnings("serial")
public class Day implements Serializable{
 
    /** Associations */
    private Pointing arrivingPointing;
    private Pointing leavingPointing;

    /**
     * getDate
     * @return Date
     */
	public Date getDate() {
		return arrivingPointing.getDate();
	}
	
	/**
	 * getEmployee
	 * @return Employee
	 */
	public Employee getEmployee() {
		return arrivingPointing.getEmployee();
	}
	
	/**
	 * getArrivingPointing
	 * @return Pointing arrivingPointing
	 */
	public Pointing getArrivingPointing() {
		return arrivingPointing;
	}

	public void setArrivingPointing(Pointing arrivingPointing) {
		this.arrivingPointing = arrivingPointing;
	}

	/**
	 * getLeavingPointing
	 * @return Pointing leavingPointing
	 */
	public Pointing getLeavingPointing() {
		return leavingPointing;
	}
	
	public void setLeavingPointing(Pointing leavingPointing) {
		this.leavingPointing = leavingPointing;
	}

	/**
	 * Day constructor
	 */
	public Day() {
		super();
	}

	/**
	 * Day constructor
	 * @param pointings
	 */
	public Day(ArrayList<Pointing> pointings) {
		for(Pointing p: pointings) {
			addPointing(p);
		}
	}

	/**
	 * Day constructor
	 * @param arrivingPointing
	 * @param leavingPointing
	 */
	public Day(Pointing arrivingPointing, Pointing leavingPointing) {
		addPointing(arrivingPointing);
		addPointing(leavingPointing);
	}

	/**
	 * toString
	 */
	@Override
	public String toString() {
		String retour = "\n";
		retour += getEmployee().getName() +" " + getEmployee().getFirstName() + " " + getDate().getLdt().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))+ "\n";
		retour += arrivingPointing+"\n";
		retour += leavingPointing+"\n";
		return retour;
	}

	/**
	 * addPointing
	 * @param pointing
	 */
	public void addPointing(Pointing pointing) {
		/**
		 * if pointing is null
		 * set the arriving
		 */
		if(arrivingPointing==null) {
			arrivingPointing=pointing;
		}
		/**
		 * else check if arrivingPointing > pointing
		 * update the day (arriving and leaving)
		 */
		else if(
				pointing.getDate().getLdt().getDayOfYear()==arrivingPointing.getDate().getLdt().getDayOfYear() &&
				pointing.getDate().getLdt().getHour()<arrivingPointing.getDate().getLdt().getHour() ||
				(pointing.getDate().getLdt().getHour()==arrivingPointing.getDate().getLdt().getHour() &&
				pointing.getDate().getLdt().getMinute()<=arrivingPointing.getDate().getLdt().getMinute())
				) {
			leavingPointing=arrivingPointing;
			arrivingPointing=pointing;
		}
		/**
		 * if pointing is null
		 * set the leaving 
		 */
		else if(leavingPointing==null) {
			leavingPointing=pointing;
		}
		/**
		 * else check if leavingPointing < pointing
		 * update the leaving
		 */
		else if(
				pointing.getDate().getLdt().getDayOfYear()==leavingPointing.getDate().getLdt().getDayOfYear() &&
				pointing.getDate().getLdt().getHour()>leavingPointing.getDate().getLdt().getHour() ||
				(pointing.getDate().getLdt().getHour()==arrivingPointing.getDate().getLdt().getHour() &&
				pointing.getDate().getLdt().getMinute()>=arrivingPointing.getDate().getLdt().getMinute())
				) {
			leavingPointing=pointing;
		}
	}
	
	/**
	 * updateOvertime
	 * update the Employee overtime
	 */
	public void updateOvertime() {
		/**
		 * update if arriving is not null
		 */
		if(arrivingPointing!=null){
			int tmpOvertime =	(arrivingPointing.getDate().getLdt().getMinute()-getEmployee().getArrivingHour().getLdt().getMinute()
				+
				(arrivingPointing.getDate().getLdt().getHour()-getEmployee().getArrivingHour().getLdt().getHour())
				*
				60
			);
			if(tmpOvertime<=Config.getAccidentThreshold() && tmpOvertime>=0)
				tmpOvertime=0;
			getEmployee().updateOvertime(tmpOvertime);
			
		}
			
		/**
		 * update if leaving is not null
		 */
		if(leavingPointing!=null){
			int tmpOvertime = -(leavingPointing.getDate().getLdt().getMinute()-getEmployee().getLeavingHour().getLdt().getMinute()
				+
				(leavingPointing.getDate().getLdt().getHour()-getEmployee().getLeavingHour().getLdt().getHour())
				*
				60
			);
			if(tmpOvertime<=Config.getAccidentThreshold() && tmpOvertime>=0)
				tmpOvertime=0;
			getEmployee().updateOvertime(tmpOvertime);
		}
	}

}
 
