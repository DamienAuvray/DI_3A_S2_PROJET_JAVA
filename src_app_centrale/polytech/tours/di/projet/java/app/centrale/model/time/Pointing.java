package polytech.tours.di.projet.java.app.centrale.model.time;

import java.io.Serializable;

import polytech.tours.di.projet.java.app.centrale.model.company.Employee;

/**
 * Pointing
 * @author Damien Auvray
 *
 */
@SuppressWarnings("serial")
public class Pointing implements Serializable{

    /** Associations */
    private Employee employee = null;
    private Date date = null;
    
    /**
     * Pointing
     * @param Employee employee
     */
    public Pointing(Employee employee){
    	this.employee = employee;
    	this.date = new Date();
    }
    
    /**
     * Pointing constructor
     * @param employee
     * @param date
     */
    public Pointing(Employee employee, Date date){
    	this.employee = employee;
    	this.date = date;
    }
    
    /**
     * Pointing constructor
     * @param pointing
     */
	public Pointing(Pointing pointing) {
		this.employee = pointing.getEmployee();
    	this.date = pointing.getDate();
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}
	
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the employee
	 */
	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	/**
	 * toString
	 */
	@Override
	public String toString() {
		return "Pointing [employee=" + employee.getFirstName() + " " + employee.getName() + ", date=" + date + "]";
	}
	

}

// ## Implementation preserve start class closing. 
// ## Implementation preserve end class closing. 
