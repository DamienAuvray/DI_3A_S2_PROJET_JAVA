package polytech.tours.di.projet.java.app.centrale.test;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;

import org.junit.Before;
import org.junit.Test;

import polytech.tours.di.projet.java.app.centrale.model.company.*;
import polytech.tours.di.projet.java.app.centrale.model.time.Date;

/**
 * CompanyTest
 * @author Damien Auvray
 *
 */
public class CompanyTest {
	private Company company;
	
	/**
	 * SetUp() tests.
	 * @throws Exception
	 */
	@Before
    public void setUp() throws Exception {
		company = new Company();
		try {
			company.importCSV(new File("EmployeesFiles/importCSV.csv"));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		try {
			company.exportCSV(new File("EmployeesFiles/exportCSV.csv"));
		} catch (IOException e) {
			e.printStackTrace();
		}
    }

	/**
	 * Test Import
	 */
	@Test
	public void testImportExport() {
		/**Le fichier d'export contient que 2 employes.*/
	    int linenumber = 0;
	    try{
    		File file =new File("EmployeesFiles/importCSV.csv");
    		if(file.exists()){
    		    FileReader fr = new FileReader(file);
    		    LineNumberReader lnr = new LineNumberReader(fr);
    	            while (lnr.readLine() != null)
    	            	linenumber++; 	 
    	            lnr.close();
    		}
    	}catch(IOException e){
    		e.printStackTrace();
    	}
		assertEquals(linenumber,company.getEmployees().size());
	}
	
	/**
	 * Check Manager management.
	 * Check if there is only one manager in a department.
	 */
	@Test
	public void testManager() {
		company.getDepartment("accounting").setLeader(new Manager("Leroy", "Valentin", new Date("2004-02-10 08:00"), new Date("2004-02-10 19:00"), "loic.gervois@etu.univ-tours.fr"));
		/**there is already a manager, so do nothing*/
		company.getDepartment("accounting").setLeader(new Manager("Gervois", "Loic", new Date("2004-02-10 08:00"), new Date("2004-02-10 19:00"), "loic.gervois@etu.univ-tours.fr"));
		//System.out.println( company.getDepartment("accounting").getLeader().toString());
		assertEquals(company.getEmployee("Leroy", "Valentin"), company.getDepartment("accounting").getLeader());
		/**remove*/
		company.getDepartment("accounting").removeLeader();
		assertEquals(null, company.getDepartment("accounting").getLeader());
	}
	
	/**
	 * Test on Employees.
	 * Check that there is only one employee call Leroy Valentin in this company.
	 */
	@Test
	public void testEmployee() {
		company.addEmployee("Leroy", "Valentin",new Date("2004-02-10 08:00"), new Date("2004-02-10 19:00"), "accounting");
		/**already exists*/
		company.addEmployee("Leroy", "Valentin",new Date("2004-02-10 08:00"), new Date("2004-02-10 19:00"), "accounting");
		/**already exists*/
		company.addEmployee("Leroy", "Valentin",new Date("2004-02-10 08:00"), new Date("2004-02-10 19:00"), "service");
		assertEquals(company.getEmployee("Leroy", "Valentin"), company.getDepartment("accounting").getEmployee("Leroy", "Valentin"));
		assertEquals(true, company.getDepartment("accounting").employeeExist(new Employee("Leroy", "Valentin", new Date("2004-02-10 08:00"), new Date("2004-02-10 19:00"))));
		assertEquals(false, company.getDepartment("service").employeeExist(new Employee("Leroy", "Valentin", new Date("2004-02-10 08:00"), new Date("2004-02-10 19:00"))));

		/**removes*/
		company.getDepartment("accounting").removeEmployee(new Employee("Leroy", "Valentin", new Date("2004-02-10 08:00"), new Date("2004-02-10 19:00")));
		
		assertEquals(false, company.getDepartment("accounting").employeeExist("Leroy", "Valentin"));
	}
	
	/**
	 * Test on Department.
	 * Check Add and Remove methods.
	 */
	@Test
	public void testDepartement() {
		company.addDepartment("technology");
		company.addDepartment("finance");
		/**already exists*/
		company.addDepartment("accounting");
		assertEquals(4, company.getDepartment().size());
		company.removeDepartment(company.getDepartment("technology"));
		assertEquals(3, company.getDepartment().size());
	}

}
