package polytech.tours.di.projet.java.app.centrale.test;

import static org.junit.Assert.*;

import java.time.LocalDateTime;

import org.junit.Test;

import polytech.tours.di.projet.java.app.centrale.model.time.Date;

/**
 * DateTest
 * @author Damien Auvray
 *
 */
public class DateTest {

	/**
	 * Test on Create.
	 */
	@Test
	public void testCreateDate() {
		Date ldt_int = new Date();
		Date ldt_string = new Date();
		
		ldt_int = new Date(2018,04,18,16,0);
		ldt_string = new Date("2018-04-18 16:00");
		assertEquals("2018-04-18 16:00",ldt_int.toString());
		assertEquals(ldt_string.toString(),ldt_int.toString());
		
		ldt_int = new Date(2018,04,18,16,7);
		ldt_string = new Date("2018-04-18 16:07");
		assertEquals("2018-04-18 16:00",ldt_int.toString());
		assertEquals(ldt_string.toString(),ldt_int.toString());
		
		ldt_int = new Date(2018,04,18,16,8);
		ldt_string = new Date("2018-04-18 16:08");
		assertEquals("2018-04-18 16:15",ldt_int.toString());	
		assertEquals(ldt_string.toString(),ldt_int.toString());
			
		ldt_int = new Date(2018,04,18,16,15);
		ldt_string = new Date("2018-04-18 16:15");
		assertEquals("2018-04-18 16:15",ldt_int.toString());
		assertEquals(ldt_string.toString(),ldt_int.toString());
				
		ldt_int = new Date(2018,04,18,16,22);
		ldt_string = new Date("2018-04-18 16:22");
		assertEquals("2018-04-18 16:15",ldt_int.toString());
		assertEquals(ldt_string.toString(),ldt_int.toString());
				
		ldt_int = new Date(2018,04,18,16,23);
		ldt_string = new Date("2018-04-18 16:23");
		assertEquals("2018-04-18 16:30",ldt_int.toString());
		assertEquals(ldt_string.toString(),ldt_int.toString());
				
		ldt_int = new Date(2018,04,18,16,30);
		ldt_string = new Date("2018-04-18 16:30");
		assertEquals("2018-04-18 16:30",ldt_int.toString());
		assertEquals(ldt_string.toString(),ldt_int.toString());
				
		ldt_int  = new Date(2018,04,18,16,37);
		ldt_string = new Date("2018-04-18 16:37");
		assertEquals("2018-04-18 16:30",ldt_int.toString());
		assertEquals(ldt_string.toString(),ldt_int.toString());
				
		ldt_int = new Date(2018,04,18,16,38);
		ldt_string = new Date("2018-04-18 16:38");
		assertEquals("2018-04-18 16:45",ldt_int.toString());
		assertEquals(ldt_string.toString(),ldt_int.toString());
				
		ldt_int = new Date(2018,04,18,16,45);
		ldt_string = new Date("2018-04-18 16:45");
		assertEquals("2018-04-18 16:45",ldt_int.toString());
		assertEquals(ldt_string.toString(),ldt_int.toString());
				
		ldt_int = new Date(2018,04,18,16,52);
		ldt_string = new Date("2018-04-18 16:52");
		assertEquals("2018-04-18 16:45",ldt_int.toString());
		assertEquals(ldt_string.toString(),ldt_int.toString());
		
		ldt_int = new Date(2018,04,18,16,53);
		ldt_string = new Date("2018-04-18 16:53");
		assertEquals("2018-04-18 17:00",ldt_int.toString());
		assertEquals(ldt_string.toString(),ldt_int.toString());
		
		
		ldt_int = new Date(2018,04,18,23,55);
		ldt_string = new Date("2018-04-18 23:55");
		assertEquals("2018-04-19 00:00",ldt_int.toString());
		assertEquals(ldt_string.toString(),ldt_int.toString());
		
		
		ldt_string = new Date("2018-04-18 16:00");
		ldt_int = new Date(2018,04,18,16,00);
		assertEquals(LocalDateTime.of(2018,04,18,16,00),ldt_int.getLdt());
		assertEquals(ldt_int.getLdt(),ldt_string.getLdt());
	}
	
	/**
	 * Test on Set.
	 */
	@Test
	public void testSetDate() {
		Date ldt = new Date();
		
		ldt.setLdt(LocalDateTime.of(2018,04,18,16,0));
		assertEquals("2018-04-18 16:00",ldt.toString());
		
		ldt.setLdt(LocalDateTime.of(2018,04,18,16,7));
		assertEquals("2018-04-18 16:00",ldt.toString());
		
		ldt.setLdt(LocalDateTime.of(2018,04,18,16,8));
		assertEquals("2018-04-18 16:15",ldt.toString());
		
		ldt.setLdt(LocalDateTime.of(2018,04,18,16,15));
		assertEquals("2018-04-18 16:15",ldt.toString());
		
		ldt.setLdt(LocalDateTime.of(2018,04,18,16,22));
		assertEquals("2018-04-18 16:15",ldt.toString());
		
		ldt.setLdt(LocalDateTime.of(2018,04,18,16,23));
		assertEquals("2018-04-18 16:30",ldt.toString());
		
		ldt.setLdt(LocalDateTime.of(2018,04,18,16,30));
		assertEquals("2018-04-18 16:30",ldt.toString());
		
		ldt.setLdt(LocalDateTime.of(2018,04,18,16,37));
		assertEquals("2018-04-18 16:30",ldt.toString());
		
		ldt.setLdt(LocalDateTime.of(2018,04,18,16,38));
		assertEquals("2018-04-18 16:45",ldt.toString());
		
		ldt.setLdt(LocalDateTime.of(2018,04,18,16,45));
		assertEquals("2018-04-18 16:45",ldt.toString());
		
		ldt.setLdt(LocalDateTime.of(2018,04,18,16,52));
		assertEquals("2018-04-18 16:45",ldt.toString());
		
		ldt.setLdt(LocalDateTime.of(2018,04,18,16,53));
		assertEquals("2018-04-18 17:00",ldt.toString());
		
		ldt.setLdt(LocalDateTime.of(2018,04,18,23,55));
		assertEquals("2018-04-19 00:00",ldt.toString());
	}

}
