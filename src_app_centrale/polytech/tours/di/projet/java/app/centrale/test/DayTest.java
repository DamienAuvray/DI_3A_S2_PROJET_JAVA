package polytech.tours.di.projet.java.app.centrale.test;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import polytech.tours.di.projet.java.app.centrale.model.company.Company;
import polytech.tours.di.projet.java.app.centrale.model.company.Manager;
import polytech.tours.di.projet.java.app.centrale.model.time.Date;
import polytech.tours.di.projet.java.app.centrale.model.time.Pointing;

/**
 * DayTest
 * @author Damien Auvray
 *
 */
public class DayTest {

	private Company company;
	
	/**
	 * SetUp() Tests.
	 * @throws Exception
	 */
	@Before
    public void setUp() throws Exception {
		company = new Company();
		try {
			company.importCSV(new File("EmployeesFiles/importCSV.csv"));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		company.getDepartment("accounting").setLeader(new Manager("Leroy", "Valentin", new Date("2004-02-10 08:00"), new Date("2004-02-10 19:00"), "loic.gervois@etu.univ-tours.fr"));
		try {
			company.exportCSV(new File("EmployeesFiles/exportCSV.csv"));
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
	
	/**
	 * Test Day.
	 */
	@Test
	public void testDay() {		
		/**
		 * Add few days from a list of many pointing.
		 */
		ArrayList<Pointing> pointings = new ArrayList<Pointing>();
		/**
		 * Here a employee failed and point 3 time.
		 * Donovan: +60min overtime.
		 */
		pointings.add(new Pointing(company.getEmployee("Guillot", "Donovan"), new Date("2004-03-02 08:00")));
		pointings.add(new Pointing(company.getEmployee("Guillot", "Donovan"), new Date("2004-03-02 17:00")));
		pointings.add(new Pointing(company.getEmployee("Guillot", "Donovan"), new Date("2004-03-02 19:00")));
		
		pointings.add(new Pointing(company.getEmployee("Auvray", "Damien"), new Date("2004-03-02 08:00")));
		pointings.add(new Pointing(company.getEmployee("Auvray", "Damien"), new Date("2004-03-02 18:00")));
		/**
		 * Pointing mixed.
		 */
		pointings.add(new Pointing(company.getEmployee("Guillot", "Donovan"), new Date("2004-03-03 18:00")));
		pointings.add(new Pointing(company.getEmployee("Guillot", "Donovan"), new Date("2004-03-03 08:00")));
		/**
		 * Pointing Manager
		 */
		pointings.add(new Pointing(company.getLeader("Leroy", "Valentin"), new Date("2004-03-03 08:00")));
		pointings.add(new Pointing(company.getLeader("Leroy", "Valentin"), new Date("2004-03-03 18:00")));
		company.addDay(pointings);
		
		assertEquals(4, company.getDays().size());
		/**
		 * check overtime
		 */
		System.out.println(company);
		assertEquals(60, company.getEmployee("Leroy", "Valentin").getOvertime());
		assertEquals(-60, company.getEmployee("Guillot", "Donovan").getOvertime());
	}

}
