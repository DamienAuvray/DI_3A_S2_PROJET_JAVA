package polytech.tours.di.projet.java.app.centrale.view;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;

/**
 * AddDepartmentView
 * @author Damien Auvray
 *
 */
@SuppressWarnings("serial")
public class AddDepartmentView extends JDialog {
	
	private JPanel panel;
	private JLabel lblNom;
	private JTextField txtNom;
	private JLabel lblManager;
	private JComboBox<Object> cbManager;
	private JButton btnAdd;
	private JButton btnClose;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			AddDepartmentView dialog = new AddDepartmentView();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public AddDepartmentView() {
		setTitle("Add Department - V1");
		setBounds(100, 100, 450, 300);
		
		panel = new JPanel();
		panel.setLayout(null);
		panel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(panel, BorderLayout.CENTER);
		
		lblNom = new JLabel("Nom:");
		lblNom.setBounds(10, 11, 90, 14);
		panel.add(lblNom);
		
		txtNom = new JTextField();
		txtNom.setColumns(10);
		txtNom.setBounds(110, 8, 314, 20);
		panel.add(txtNom);
		
		lblManager = new JLabel("Manager:");
		lblManager.setBounds(10, 39, 90, 14);
		panel.add(lblManager);
		
		cbManager = new JComboBox<Object>();
		cbManager.setBounds(110, 39, 314, 20);
		panel.add(cbManager);
		
		btnAdd = new JButton("Add");
		btnAdd.setBounds(236, 227, 89, 23);
		panel.add(btnAdd);
		
		btnClose = new JButton("Close");
		btnClose.setBounds(335, 227, 89, 23);
		panel.add(btnClose);
		
		setVisible(true);
	}

	/**
	 * getPanel
	 * @return panel
	 */
	public JPanel getPanel() {
		return panel;
	}

	/**
	 * setPanel
	 * @param panel
	 */
	public void setPanel(JPanel panel) {
		this.panel = panel;
	}

	/**
	 * getLblNom
	 * @return lblNom
	 */
	public JLabel getLblNom() {
		return lblNom;
	}

	/**
	 * setLblNom
	 * @param lblNom
	 */
	public void setLblNom(JLabel lblNom) {
		this.lblNom = lblNom;
	}

	/**
	 * getTxtNom
	 * @return txtNom
	 */
	public JTextField getTxtNom() {
		return txtNom;
	}

	/**
	 * setTxtNom
	 * @param txtNom
	 */
	public void setTxtNom(JTextField txtNom) {
		this.txtNom = txtNom;
	}

	/**
	 * getLblManager
	 * @return lblManager
	 */
	public JLabel getLblManager() {
		return lblManager;
	}

	/**
	 * setLblManager
	 * @param lblManager
	 */
	public void setLblManager(JLabel lblManager) {
		this.lblManager = lblManager;
	}

	/**
	 * getCbManager
	 * @return cbManager
	 */
	public JComboBox<Object> getCbManager() {
		return cbManager;
	}

	/**
	 * setCbManager
	 * @param cbManager
	 */
	public void setCbManager(JComboBox<Object> cbManager) {
		this.cbManager = cbManager;
	}

	/**
	 * getBtnAdd
	 * @return btnAdd
	 */
	public JButton getBtnAdd() {
		return btnAdd;
	}

	/**
	 * setBtnAdd
	 * @param btnAdd
	 */
	public void setBtnAdd(JButton btnAdd) {
		this.btnAdd = btnAdd;
	}

	/**
	 * getBtnClose
	 * @return btnClose
	 */
	public JButton getBtnClose() {
		return btnClose;
	}

	/**
	 * setBtnClose
	 * @param btnClose
	 */
	public void setBtnClose(JButton btnClose) {
		this.btnClose = btnClose;
	}
	
	
}
