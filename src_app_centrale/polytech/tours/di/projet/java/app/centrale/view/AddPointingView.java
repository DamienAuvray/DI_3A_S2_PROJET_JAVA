package polytech.tours.di.projet.java.app.centrale.view;

import java.awt.BorderLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JComboBox;

/**
 * AddPointingView
 * @author Damien Auvray
 *
 */
@SuppressWarnings("serial")
public class AddPointingView extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JButton btnAdd;
	private JButton btnClose;
	private ButtonGroup bG;
	private JFormattedTextField txtDate;
	private JComboBox<Object> cbEmployee;
	private JLabel lblDate;
	private JLabel lblEmployee;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			AddPointingView dialog = new AddPointingView();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public AddPointingView() {
		setTitle("Add Pointing - V1");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		btnAdd = new JButton("Add");
		btnAdd.setBounds(236, 227, 89, 23);
		contentPanel.add(btnAdd);
	
	
		btnClose = new JButton("Close");
		btnClose.setBounds(335, 227, 89, 23);
		contentPanel.add(btnClose);
	
	
		lblDate = new JLabel("Date:");
		lblDate.setBounds(10, 42, 90, 14);
		contentPanel.add(lblDate);
	
	
		txtDate = new JFormattedTextField();
		txtDate.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter(new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm"))));
		txtDate.setColumns(10);
		txtDate.setBounds(110, 39, 314, 20);
		contentPanel.add(txtDate);
	
	
		cbEmployee = new JComboBox<Object>();
		cbEmployee.setBounds(110, 11, 314, 20);
		contentPanel.add(cbEmployee);
	
	
		JLabel lblEmployee = new JLabel("Employee:");
		lblEmployee.setBounds(10, 14, 90, 14);
		contentPanel.add(lblEmployee);
		
		setVisible(true);
		
	}

	/**
	 * @return the btnAdd
	 */
	public JButton getBtnAdd() {
		return btnAdd;
	}

	/**
	 * @param btnAdd the btnAdd to set
	 */
	public void setBtnAdd(JButton btnAdd) {
		this.btnAdd = btnAdd;
	}

	/**
	 * @return the btnClose
	 */
	public JButton getBtnClose() {
		return btnClose;
	}

	/**
	 * @param btnClose the btnClose to set
	 */
	public void setBtnClose(JButton btnClose) {
		this.btnClose = btnClose;
	}

	/**
	 * @return the bG
	 */
	public ButtonGroup getbG() {
		return bG;
	}

	/**
	 * @param bG the bG to set
	 */
	public void setbG(ButtonGroup bG) {
		this.bG = bG;
	}

	/**
	 * @return the txtDate
	 */
	public JFormattedTextField getTxtDate() {
		return txtDate;
	}

	/**
	 * @param txtDate the txtDate to set
	 */
	public void setTxtDate(JFormattedTextField txtDate) {
		this.txtDate = txtDate;
	}

	/**
	 * @return the cbEmployee
	 */
	public JComboBox<Object> getCbEmployee() {
		return cbEmployee;
	}

	/**
	 * @param cbEmployee the cbEmployee to set
	 */
	public void setCbEmployee(JComboBox<Object> cbEmployee) {
		this.cbEmployee = cbEmployee;
	}

	/**
	 * @return the lblDate
	 */
	public JLabel getLblDate() {
		return lblDate;
	}

	/**
	 * @param lblDate the lblDate to set
	 */
	public void setLblDate(JLabel lblDate) {
		this.lblDate = lblDate;
	}

	/**
	 * @return the lblEmployee
	 */
	public JLabel getLblEmployee() {
		return lblEmployee;
	}

	/**
	 * @param lblEmployee the lblEmployee to set
	 */
	public void setLblEmployee(JLabel lblEmployee) {
		this.lblEmployee = lblEmployee;
	}

	/**
	 * @return the contentPanel
	 */
	public JPanel getContentPanel() {
		return contentPanel;
	}
}
