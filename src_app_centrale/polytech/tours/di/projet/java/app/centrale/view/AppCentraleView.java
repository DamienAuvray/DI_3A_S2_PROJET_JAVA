package polytech.tours.di.projet.java.app.centrale.view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.JScrollPane;

/**
 * AppCentraleView
 * @author Damien Auvray
 *
 */
public class AppCentraleView {

	private JFrame frame;
	private JPanel pPointing;  
	private JPanel pDepartment;  
	private JPanel pEmployee;  
	private JPanel pSettings; 
	private JTabbedPane tp;
	private JTextField txtSearchPointing;
	private JTable tablePointing;
	private JTextField txtSearchDepartment;
	private JTable tableDepartment;
	private JTextField txtSearchEmployee;
	private JTable tableEmployee;
	private JTextField txtIPAdress;
	private JTextField txtPortEmployee;
	private JTextField txtPortPointing;
	private JTextField txtAccidentThreshold;
	private JButton btnSearchPointing;
	private JCheckBox chckbxDaillyPointingsOnly;
	private JButton btnRefreshPointing;
	private JButton btnAddPointing;
	private JButton btnAddDepartment;
	private JButton btnSearchDepartment;
	private JButton btnRefreshDepartment;
	private JButton btnAddEmployee;
	private JButton btnImportCsv;
	private JButton btnExportCsv;
	private JButton btnSearchEmployee;
	private JButton btnRefreshEmployee;
	private JLabel lblIPAdress;
	private JLabel lblPortEmployee;
	private JLabel lblPortPointing;
	private JLabel lblAccidentThreshold;
	private JLabel lblMin;
	private JButton btnUpdate;
	private JScrollPane scrollPanePointing;
	private JScrollPane scrollPaneEmployee;
	private JScrollPane scrollPaneDepartment;
	private DefaultTableModel modeltablePointing;
	private DefaultTableModel modeltableEmployee;
	private DefaultTableModel modeltableDepartment;
	private JButton btnImportPointing;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AppCentraleView window = new AppCentraleView();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	/**
	 * Initialize the contents of the frame.
	 */
	public AppCentraleView() {
		frame = new JFrame();
		frame.setTitle("Central Application - V1");
		frame.setBounds(100, 100, 482, 290);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new GridLayout(0, 1, 0, 0));
		frame.setResizable(false);
		
		   
		pPointing=new JPanel();  
		pDepartment=new JPanel();  
		pEmployee=new JPanel();  
		pSettings=new JPanel(); 
		tp=new JTabbedPane();
		tp.add("Pointing",pPointing);  
		pPointing.setLayout(null);
		
		txtSearchPointing = new JTextField();
		txtSearchPointing.setBounds(10, 12, 206, 20);
		pPointing.add(txtSearchPointing);
		txtSearchPointing.setColumns(10);
		
		btnSearchPointing = new JButton("Search");
		btnSearchPointing.setBounds(226, 11, 85, 23);
		pPointing.add(btnSearchPointing);
		
		chckbxDaillyPointingsOnly = new JCheckBox("Dailly Pointings Only");
		chckbxDaillyPointingsOnly.setBounds(317, 11, 152, 23);
		pPointing.add(chckbxDaillyPointingsOnly);
		
		btnRefreshPointing = new JButton("Refresh");
		btnRefreshPointing.setBounds(376, 199, 85, 23);
		pPointing.add(btnRefreshPointing);
		
		btnAddPointing = new JButton("Add Pointing");
		btnAddPointing.setBounds(10, 199, 112, 23);
		pPointing.add(btnAddPointing);
		
		btnImportPointing = new JButton("Import CSV");
		btnImportPointing.setBounds(132, 199, 112, 23);
		pPointing.add(btnImportPointing);
		
		scrollPanePointing = new JScrollPane();
		scrollPanePointing.setBounds(10, 42, 451, 146);
		pPointing.add(scrollPanePointing);
		
		tablePointing = new JTable();
		scrollPanePointing.setViewportView(tablePointing);
		
		modeltablePointing = new DefaultTableModel() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int column) {       
				return false; // or a condition at your choice with row and column
			}
		};
		getTablePointing().setModel(modeltablePointing);		
		
		tp.add("Department",pDepartment);  
		pDepartment.setLayout(null);
		
		txtSearchDepartment = new JTextField();
		txtSearchDepartment.setColumns(10);
		txtSearchDepartment.setBounds(10, 12, 206, 20);
		pDepartment.add(txtSearchDepartment);
		
		scrollPaneDepartment = new JScrollPane();
		scrollPaneDepartment.setBounds(10, 42, 451, 146);
		pDepartment.add(scrollPaneDepartment);
		
		tableDepartment = new JTable();
		scrollPaneDepartment.setViewportView(tableDepartment);
		
		modeltableDepartment = new DefaultTableModel() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int column) {       
				return false; // or a condition at your choice with row and column
			}
		};
		getTableDepartment().setModel(modeltableDepartment);
		
		btnAddDepartment = new JButton("Add Department");
		btnAddDepartment.setBounds(321, 11, 140, 23);
		pDepartment.add(btnAddDepartment);
		
		btnSearchDepartment = new JButton("Search");
		btnSearchDepartment.setBounds(226, 11, 85, 23);
		pDepartment.add(btnSearchDepartment);
		
		btnRefreshDepartment = new JButton("Refresh");
		btnRefreshDepartment.setBounds(376, 199, 85, 23);
		pDepartment.add(btnRefreshDepartment);
		
		
		tp.add("Employee",pEmployee);    
		pEmployee.setLayout(null);
		
		txtSearchEmployee = new JTextField();
		txtSearchEmployee.setColumns(10);
		txtSearchEmployee.setBounds(10, 12, 206, 20);
		pEmployee.add(txtSearchEmployee);
		
		btnAddEmployee = new JButton("Add Employee");
		btnAddEmployee.setBounds(321, 11, 140, 23);
		pEmployee.add(btnAddEmployee);
		
		btnImportCsv = new JButton("Import CSV");
		btnImportCsv.setBounds(10, 199, 112, 23);
		pEmployee.add(btnImportCsv);
		
		btnExportCsv = new JButton("Export CSV");
		btnExportCsv.setBounds(132, 199, 112, 23);
		pEmployee.add(btnExportCsv);
		
		btnSearchEmployee = new JButton("Search");
		btnSearchEmployee.setBounds(226, 11, 85, 23);
		pEmployee.add(btnSearchEmployee);
		
		btnRefreshEmployee = new JButton("Refresh");
		btnRefreshEmployee.setBounds(376, 199, 85, 23);
		pEmployee.add(btnRefreshEmployee);
		
		scrollPaneEmployee = new JScrollPane();
		scrollPaneEmployee.setBounds(10, 42, 451, 146);
		pEmployee.add(scrollPaneEmployee);
		
		tableEmployee = new JTable();
		scrollPaneEmployee.setViewportView(tableEmployee);
		
		modeltableEmployee = new DefaultTableModel() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int column) {       
				return false; // or a condition at your choice with row and column
			}
		};
		getTableEmployee().setModel(modeltableEmployee);	
		
		tp.add("Settings",pSettings);    
		pSettings.setLayout(null);
		
		lblIPAdress = new JLabel("IP Adress:");
		lblIPAdress.setBounds(10, 11, 75, 14);
		pSettings.add(lblIPAdress);
		
		txtIPAdress = new JTextField();
		txtIPAdress.setBounds(145, 8, 200, 20);
		pSettings.add(txtIPAdress);
		txtIPAdress.setColumns(10);
		
		lblPortEmployee = new JLabel("Port Employee:");
		lblPortEmployee.setBounds(10, 42, 125, 14);
		pSettings.add(lblPortEmployee);
		
		lblPortPointing = new JLabel("Port Pointing:");
		lblPortPointing.setBounds(10, 73, 125, 14);
		pSettings.add(lblPortPointing);
		
		lblAccidentThreshold = new JLabel("Accident threshold:");
		lblAccidentThreshold.setBounds(10, 104, 125, 14);
		pSettings.add(lblAccidentThreshold);
		
		txtPortEmployee = new JTextField();
		txtPortEmployee.setColumns(10);
		txtPortEmployee.setBounds(145, 39, 200, 20);
		pSettings.add(txtPortEmployee);
		
		txtPortPointing = new JTextField();
		txtPortPointing.setColumns(10);
		txtPortPointing.setBounds(145, 70, 200, 20);
		pSettings.add(txtPortPointing);
		
		txtAccidentThreshold = new JTextField();
		txtAccidentThreshold.setColumns(10);
		txtAccidentThreshold.setBounds(145, 101, 57, 20);
		pSettings.add(txtAccidentThreshold);
		
		lblMin = new JLabel("min");
		lblMin.setBounds(212, 73, 46, 14);
		pSettings.add(lblMin);
		
		btnUpdate = new JButton("Update");
		btnUpdate.setBounds(372, 199, 89, 23);
		pSettings.add(btnUpdate);
		
		frame.getContentPane().add(tp);
		
		frame.setVisible(true);
	}


	/**
	 * @return the frame
	 */
	public JFrame getFrame() {
		return frame;
	}


	/**
	 * @param frame the frame to set
	 */
	public void setFrame(JFrame frame) {
		this.frame = frame;
	}


	/**
	 * @return the pPointing
	 */
	public JPanel getpPointing() {
		return pPointing;
	}


	/**
	 * @param pPointing the pPointing to set
	 */
	public void setpPointing(JPanel pPointing) {
		this.pPointing = pPointing;
	}


	/**
	 * @return the pDepartment
	 */
	public JPanel getpDepartment() {
		return pDepartment;
	}


	/**
	 * @param pDepartment the pDepartment to set
	 */
	public void setpDepartment(JPanel pDepartment) {
		this.pDepartment = pDepartment;
	}


	/**
	 * @return the pEmployee
	 */
	public JPanel getpEmployee() {
		return pEmployee;
	}


	/**
	 * @param pEmployee the pEmployee to set
	 */
	public void setpEmployee(JPanel pEmployee) {
		this.pEmployee = pEmployee;
	}


	/**
	 * @return the pSettings
	 */
	public JPanel getpSettings() {
		return pSettings;
	}


	/**
	 * @param pSettings the pSettings to set
	 */
	public void setpSettings(JPanel pSettings) {
		this.pSettings = pSettings;
	}


	/**
	 * @return the tp
	 */
	public JTabbedPane getTp() {
		return tp;
	}


	/**
	 * @param tp the tp to set
	 */
	public void setTp(JTabbedPane tp) {
		this.tp = tp;
	}


	/**
	 * @return the txtSearchPointing
	 */
	public JTextField getTxtSearchPointing() {
		return txtSearchPointing;
	}


	/**
	 * @param txtSearchPointing the txtSearchPointing to set
	 */
	public void setTxtSearchPointing(JTextField txtSearchPointing) {
		this.txtSearchPointing = txtSearchPointing;
	}


	/**
	 * @return the tablePointing
	 */
	public JTable getTablePointing() {
		return tablePointing;
	}


	/**
	 * @param tablePointing the tablePointing to set
	 */
	public void setTablePointing(JTable tablePointing) {
		this.tablePointing = tablePointing;
	}


	/**
	 * @return the txtSearchDepartment
	 */
	public JTextField getTxtSearchDepartment() {
		return txtSearchDepartment;
	}


	/**
	 * @param txtSearchDepartment the txtSearchDepartment to set
	 */
	public void setTxtSearchDepartment(JTextField txtSearchDepartment) {
		this.txtSearchDepartment = txtSearchDepartment;
	}


	/**
	 * @return the tableDepartment
	 */
	public JTable getTableDepartment() {
		return tableDepartment;
	}


	/**
	 * @param tableDepartment the tableDepartment to set
	 */
	public void setTableDepartment(JTable tableDepartment) {
		this.tableDepartment = tableDepartment;
	}


	/**
	 * @return the txtSearchEmployee
	 */
	public JTextField getTxtSearchEmployee() {
		return txtSearchEmployee;
	}


	/**
	 * @param txtSearchEmployee the txtSearchEmployee to set
	 */
	public void setTxtSearchEmployee(JTextField txtSearchEmployee) {
		this.txtSearchEmployee = txtSearchEmployee;
	}


	/**
	 * @return the tableEmployee
	 */
	public JTable getTableEmployee() {
		return tableEmployee;
	}


	/**
	 * @param tableEmployee the tableEmployee to set
	 */
	public void setTableEmployee(JTable tableEmployee) {
		this.tableEmployee = tableEmployee;
	}


	/**
	 * @return the txtIPAdress
	 */
	public JTextField getTxtIPAdress() {
		return txtIPAdress;
	}


	/**
	 * @param txtIPAdress the txtIPAdress to set
	 */
	public void setTxtIPAdress(JTextField txtIPAdress) {
		this.txtIPAdress = txtIPAdress;
	}


	/**
	 * @return the txtPortEmployee
	 */
	public JTextField getTxtPortEmployee() {
		return txtPortEmployee;
	}


	/**
	 * @param txtPortEmployee the txtPortEmployee to set
	 */
	public void setTxtPortEmployee(JTextField txtPortEmployee) {
		this.txtPortEmployee = txtPortEmployee;
	}


	/**
	 * @return the txtPortPointing
	 */
	public JTextField getTxtPortPointing() {
		return txtPortPointing;
	}


	/**
	 * @param txtPortPointing the txtPortPointing to set
	 */
	public void setTxtPortPointing(JTextField txtPortPointing) {
		this.txtPortPointing = txtPortPointing;
	}


	/**
	 * @return the txtAccidentThreshold
	 */
	public JTextField getTxtAccidentThreshold() {
		return txtAccidentThreshold;
	}


	/**
	 * @param txtAccidentThreshold the txtAccidentThreshold to set
	 */
	public void setTxtAccidentThreshold(JTextField txtAccidentThreshold) {
		this.txtAccidentThreshold = txtAccidentThreshold;
	}


	/**
	 * @return the btnSearchPointing
	 */
	public JButton getBtnSearchPointing() {
		return btnSearchPointing;
	}


	/**
	 * @param btnSearchPointing the btnSearchPointing to set
	 */
	public void setBtnSearchPointing(JButton btnSearchPointing) {
		this.btnSearchPointing = btnSearchPointing;
	}


	/**
	 * @return the chckbxDaillyPointingsOnly
	 */
	public JCheckBox getChckbxDaillyPointingsOnly() {
		return chckbxDaillyPointingsOnly;
	}


	/**
	 * @param chckbxDaillyPointingsOnly the chckbxDaillyPointingsOnly to set
	 */
	public void setChckbxDaillyPointingsOnly(JCheckBox chckbxDaillyPointingsOnly) {
		this.chckbxDaillyPointingsOnly = chckbxDaillyPointingsOnly;
	}


	/**
	 * @return the btnRefreshPointing
	 */
	public JButton getBtnRefreshPointing() {
		return btnRefreshPointing;
	}


	/**
	 * @param btnRefreshPointing the btnRefreshPointing to set
	 */
	public void setBtnRefreshPointing(JButton btnRefreshPointing) {
		this.btnRefreshPointing = btnRefreshPointing;
	}


	/**
	 * @return the btnAddPointing
	 */
	public JButton getBtnAddPointing() {
		return btnAddPointing;
	}


	/**
	 * @param btnAddPointing the btnAddPointing to set
	 */
	public void setBtnAddPointing(JButton btnAddPointing) {
		this.btnAddPointing = btnAddPointing;
	}


	/**
	 * @return the btnAddDepartment
	 */
	public JButton getBtnAddDepartment() {
		return btnAddDepartment;
	}


	/**
	 * @param btnAddDepartment the btnAddDepartment to set
	 */
	public void setBtnAddDepartment(JButton btnAddDepartment) {
		this.btnAddDepartment = btnAddDepartment;
	}


	/**
	 * @return the btnSearchDepartment
	 */
	public JButton getBtnSearchDepartment() {
		return btnSearchDepartment;
	}


	/**
	 * @param btnSearchDepartment the btnSearchDepartment to set
	 */
	public void setBtnSearchDepartment(JButton btnSearchDepartment) {
		this.btnSearchDepartment = btnSearchDepartment;
	}


	/**
	 * @return the btnRefreshDepartment
	 */
	public JButton getBtnRefreshDepartment() {
		return btnRefreshDepartment;
	}


	/**
	 * @param btnRefreshDepartment the btnRefreshDepartment to set
	 */
	public void setBtnRefreshDepartment(JButton btnRefreshDepartment) {
		this.btnRefreshDepartment = btnRefreshDepartment;
	}


	/**
	 * @return the btnAddEmployee
	 */
	public JButton getBtnAddEmployee() {
		return btnAddEmployee;
	}


	/**
	 * @param btnAddEmployee the btnAddEmployee to set
	 */
	public void setBtnAddEmployee(JButton btnAddEmployee) {
		this.btnAddEmployee = btnAddEmployee;
	}


	/**
	 * @return the btnImportCsv
	 */
	public JButton getBtnImportCsv() {
		return btnImportCsv;
	}


	/**
	 * @param btnImportCsv the btnImportCsv to set
	 */
	public void setBtnImportCsv(JButton btnImportCsv) {
		this.btnImportCsv = btnImportCsv;
	}


	/**
	 * @return the btnExportCsv
	 */
	public JButton getBtnExportCsv() {
		return btnExportCsv;
	}


	/**
	 * @param btnExportCsv the btnExportCsv to set
	 */
	public void setBtnExportCsv(JButton btnExportCsv) {
		this.btnExportCsv = btnExportCsv;
	}


	/**
	 * @return the btnSearchEmployee
	 */
	public JButton getBtnSearchEmployee() {
		return btnSearchEmployee;
	}


	/**
	 * @param btnSearchEmployee the btnSearchEmployee to set
	 */
	public void setBtnSearchEmployee(JButton btnSearchEmployee) {
		this.btnSearchEmployee = btnSearchEmployee;
	}


	/**
	 * @return the btnRefreshEmployee
	 */
	public JButton getBtnRefreshEmployee() {
		return btnRefreshEmployee;
	}


	/**
	 * @param btnRefreshEmployee the btnRefreshEmployee to set
	 */
	public void setBtnRefreshEmployee(JButton btnRefreshEmployee) {
		this.btnRefreshEmployee = btnRefreshEmployee;
	}


	/**
	 * @return the lblIPAdress
	 */
	public JLabel getLblIPAdress() {
		return lblIPAdress;
	}


	/**
	 * @param lblIPAdress the lblIPAdress to set
	 */
	public void setLblIPAdress(JLabel lblIPAdress) {
		this.lblIPAdress = lblIPAdress;
	}


	/**
	 * @return the lblPortEmployee
	 */
	public JLabel getLblPortEmployee() {
		return lblPortEmployee;
	}


	/**
	 * @param lblPortEmployee the lblPortEmployee to set
	 */
	public void setLblPortEmployee(JLabel lblPortEmployee) {
		this.lblPortEmployee = lblPortEmployee;
	}


	/**
	 * @return the lblPortPointing
	 */
	public JLabel getLblPortPointing() {
		return lblPortPointing;
	}


	/**
	 * @param lblPortPointing the lblPortPointing to set
	 */
	public void setLblPortPointing(JLabel lblPortPointing) {
		this.lblPortPointing = lblPortPointing;
	}


	/**
	 * @return the lblAccidentThreshold
	 */
	public JLabel getLblAccidentThreshold() {
		return lblAccidentThreshold;
	}


	/**
	 * @param lblAccidentThreshold the lblAccidentThreshold to set
	 */
	public void setLblAccidentThreshold(JLabel lblAccidentThreshold) {
		this.lblAccidentThreshold = lblAccidentThreshold;
	}


	/**
	 * @return the lblMin
	 */
	public JLabel getLblMin() {
		return lblMin;
	}


	/**
	 * @param lblMin the lblMin to set
	 */
	public void setLblMin(JLabel lblMin) {
		this.lblMin = lblMin;
	}


	/**
	 * @return the btnUpdate
	 */
	public JButton getBtnUpdate() {
		return btnUpdate;
	}


	/**
	 * @param btnUpdate the btnUpdate to set
	 */
	public void setBtnUpdate(JButton btnUpdate) {
		this.btnUpdate = btnUpdate;
	}


	/**
	 * @return the scrollPanePointing
	 */
	public JScrollPane getScrollPanePointing() {
		return scrollPanePointing;
	}


	/**
	 * @param scrollPanePointing the scrollPanePointing to set
	 */
	public void setScrollPanePointing(JScrollPane scrollPanePointing) {
		this.scrollPanePointing = scrollPanePointing;
	}


	/**
	 * @return the scrollPaneEmployee
	 */
	public JScrollPane getScrollPaneEmployee() {
		return scrollPaneEmployee;
	}


	/**
	 * @param scrollPaneEmployee the scrollPaneEmployee to set
	 */
	public void setScrollPaneEmployee(JScrollPane scrollPaneEmployee) {
		this.scrollPaneEmployee = scrollPaneEmployee;
	}


	/**
	 * @return the scrollPaneDepartment
	 */
	public JScrollPane getScrollPaneDepartment() {
		return scrollPaneDepartment;
	}


	/**
	 * @param scrollPaneDepartment the scrollPaneDepartment to set
	 */
	public void setScrollPaneDepartment(JScrollPane scrollPaneDepartment) {
		this.scrollPaneDepartment = scrollPaneDepartment;
	}


	/**
	 * @return the modeltablePointing
	 */
	public DefaultTableModel getModeltablePointing() {
		return modeltablePointing;
	}


	/**
	 * @param modeltablePointing the modeltablePointing to set
	 */
	public void setModeltablePointing(DefaultTableModel modeltablePointing) {
		this.modeltablePointing = modeltablePointing;
	}


	/**
	 * @return the modeltableEmployee
	 */
	public DefaultTableModel getModeltableEmployee() {
		return modeltableEmployee;
	}


	/**
	 * @param modeltableEmployee the modeltableEmployee to set
	 */
	public void setModeltableEmployee(DefaultTableModel modeltableEmployee) {
		this.modeltableEmployee = modeltableEmployee;
	}


	/**
	 * @return the modeltableDepartment
	 */
	public DefaultTableModel getModeltableDepartment() {
		return modeltableDepartment;
	}


	/**
	 * @param modeltableDepartment the modeltableDepartment to set
	 */
	public void setModeltableDepartment(DefaultTableModel modeltableDepartment) {
		this.modeltableDepartment = modeltableDepartment;
	}


	/**
	 * @return the btnImportPointing
	 */
	public JButton getBtnImportPointing() {
		return btnImportPointing;
	}


	/**
	 * @param btnImportPointing the btnImportPointing to set
	 */
	public void setBtnImportPointing(JButton btnImportPointing) {
		this.btnImportPointing = btnImportPointing;
	}
	
}
