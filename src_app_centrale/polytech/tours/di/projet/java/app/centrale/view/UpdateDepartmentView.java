package polytech.tours.di.projet.java.app.centrale.view;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

/**
 * UpdateDepartmentView
 * @author Damien Auvray
 *
 */
@SuppressWarnings("serial")
public class UpdateDepartmentView extends JDialog {
	private JTextField txtNom;
	private JComboBox<Object> cbManager;
	private JLabel lblNom;
	private JPanel panel;
	private JLabel lblManager;
	private JButton btnUpdate;
	private JButton btnClose;
	private JButton btnDelete;

	//private final JPanel contentPanel = new JPanel();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UpdateDepartmentView dialog = new UpdateDepartmentView();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public UpdateDepartmentView() {
		setTitle("Add Department - V1");
		setBounds(100, 100, 450, 300);
		
		panel = new JPanel();
		panel.setLayout(null);
		panel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(panel, BorderLayout.CENTER);
		
		lblNom = new JLabel("Nom:");
		lblNom.setBounds(10, 11, 90, 14);
		panel.add(lblNom);
		
		txtNom = new JTextField();
		txtNom.setColumns(10);
		txtNom.setBounds(110, 8, 314, 20);
		panel.add(txtNom);
		
		lblManager = new JLabel("Manager:");
		lblManager.setBounds(10, 39, 90, 14);
		panel.add(lblManager);
		
		cbManager = new JComboBox<Object>();
		cbManager.setBounds(110, 39, 314, 20);
		panel.add(cbManager);
		
		btnUpdate = new JButton("Update");
		btnUpdate.setBounds(236, 227, 89, 23);
		panel.add(btnUpdate);
		
		btnClose = new JButton("Close");
		
		btnClose.setBounds(335, 227, 89, 23);
		panel.add(btnClose);
		
		btnDelete = new JButton("Delete");
		btnDelete.setBounds(137, 227, 89, 23);
		panel.add(btnDelete);
		
		setVisible(true);
	}

	/**
	 * @return the txtNom
	 */
	public JTextField getTxtNom() {
		return txtNom;
	}

	/**
	 * @param txtNom the txtNom to set
	 */
	public void setTxtNom(JTextField txtNom) {
		this.txtNom = txtNom;
	}

	/**
	 * @return the cbManager
	 */
	public JComboBox<Object> getCbManager() {
		return cbManager;
	}

	/**
	 * @param cbManager the cbManager to set
	 */
	public void setCbManager(JComboBox<Object> cbManager) {
		this.cbManager = cbManager;
	}

	/**
	 * @return the lblNom
	 */
	public JLabel getLblNom() {
		return lblNom;
	}

	/**
	 * @param lblNom the lblNom to set
	 */
	public void setLblNom(JLabel lblNom) {
		this.lblNom = lblNom;
	}

	/**
	 * @return the panel
	 */
	public JPanel getPanel() {
		return panel;
	}

	/**
	 * @param panel the panel to set
	 */
	public void setPanel(JPanel panel) {
		this.panel = panel;
	}

	/**
	 * @return the lblManager
	 */
	public JLabel getLblManager() {
		return lblManager;
	}

	/**
	 * @param lblManager the lblManager to set
	 */
	public void setLblManager(JLabel lblManager) {
		this.lblManager = lblManager;
	}

	/**
	 * @return the btnUpdate
	 */
	public JButton getBtnUpdate() {
		return btnUpdate;
	}

	/**
	 * @param btnUpdate the btnUpdate to set
	 */
	public void setBtnUpdate(JButton btnUpdate) {
		this.btnUpdate = btnUpdate;
	}

	/**
	 * @return the btnClose
	 */
	public JButton getBtnClose() {
		return btnClose;
	}

	/**
	 * @param btnClose the btnClose to set
	 */
	public void setBtnClose(JButton btnClose) {
		this.btnClose = btnClose;
	}

	/**
	 * @return the btnDelete
	 */
	public JButton getBtnDelete() {
		return btnDelete;
	}

	/**
	 * @param btnDelete the btnDelete to set
	 */
	public void setBtnDelete(JButton btnDelete) {
		this.btnDelete = btnDelete;
	}

}
