package polytech.tours.di.projet.java.app.centrale.view;

import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

/**
 * UpdateEmployeeView
 * @author Damien Auvray
 *
 */
@SuppressWarnings("serial")
public class UpdateEmployeeView extends JDialog {
	
	private final JPanel contentPanel = new JPanel();
	private JTextField txtNom;
	private JTextField txtPrenom;
	private JFormattedTextField txtArrivingDate;
	private JFormattedTextField txtLeavingDate;
	private JTextField txtDepartment;
	private JButton btnDelete;
	private JButton btnClose;
	private JButton btnUpdate;
	private JLabel lblDepartment;
	private JLabel lblLeavingDate;
	private JLabel lblDateArrive;
	private JLabel lblPrenom;
	private JLabel lblNom;
	private JLabel lblEmail;
	private JTextField txtEmail;
	private JLabel lblManager;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UpdateEmployeeView dialog = new UpdateEmployeeView();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public UpdateEmployeeView() {
		setTitle("Add Employee - V1");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		lblNom = new JLabel("Name:");
		lblNom.setBounds(10, 11, 90, 14);
		contentPanel.add(lblNom);
	
	
		txtNom = new JTextField();
		txtNom.setBounds(110, 8, 314, 20);
		contentPanel.add(txtNom);
		txtNom.setColumns(10);
	
	
		lblPrenom = new JLabel("First name:");
		lblPrenom.setBounds(10, 39, 90, 14);
		contentPanel.add(lblPrenom);
	
	
		txtPrenom = new JTextField();
		txtPrenom.setColumns(10);
		txtPrenom.setBounds(110, 36, 314, 20);
		contentPanel.add(txtPrenom);
	
	
		lblDateArrive = new JLabel("Arriving date:");
		lblDateArrive.setBounds(10, 67, 90, 14);
		contentPanel.add(lblDateArrive);
	
	
		txtArrivingDate = new JFormattedTextField();
		txtArrivingDate.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter(new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm"))));
		txtArrivingDate.setColumns(10);
		txtArrivingDate.setBounds(110, 64, 314, 20);
		contentPanel.add(txtArrivingDate);
	
	
		lblLeavingDate = new JLabel("Leaving date:");
		lblLeavingDate.setBounds(10, 95, 90, 14);
		contentPanel.add(lblLeavingDate);
	
	
		txtLeavingDate = new JFormattedTextField();
		txtLeavingDate.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter(new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm"))));
		txtLeavingDate.setColumns(10);
		txtLeavingDate.setBounds(110, 92, 314, 20);
		contentPanel.add(txtLeavingDate);
	
	
		lblDepartment = new JLabel("Department:");
		lblDepartment.setBounds(10, 123, 90, 14);
		contentPanel.add(lblDepartment);
	
	
		txtDepartment = new JTextField();
		txtDepartment.setColumns(10);
		txtDepartment.setBounds(110, 120, 314, 20);
		contentPanel.add(txtDepartment);
	
	
		btnUpdate = new JButton("Update");
		btnUpdate.setBounds(236, 227, 89, 23);
		contentPanel.add(btnUpdate);
	
	
		btnClose = new JButton("Close");
		btnClose.setBounds(335, 227, 89, 23);
		contentPanel.add(btnClose);
	
	
		btnDelete = new JButton("Delete");
		btnDelete.setBounds(137, 227, 89, 23);
		contentPanel.add(btnDelete);
	
	
		lblManager = new JLabel("Manager only :");
		lblManager.setBounds(10, 151, 90, 14);
		contentPanel.add(lblManager);
	
	
		lblEmail = new JLabel("eMail");
		lblEmail.setBounds(10, 179, 90, 14);
		contentPanel.add(lblEmail);
	
	
		txtEmail = new JTextField();
		txtEmail.setColumns(10);
		txtEmail.setBounds(110, 176, 314, 20);
		contentPanel.add(txtEmail);
		
		setVisible(true);
	}

	/**
	 * @return the txtNom
	 */
	public JTextField getTxtNom() {
		return txtNom;
	}

	/**
	 * @param txtNom the txtNom to set
	 */
	public void setTxtNom(JTextField txtNom) {
		this.txtNom = txtNom;
	}

	/**
	 * @return the txtPrenom
	 */
	public JTextField getTxtPrenom() {
		return txtPrenom;
	}

	/**
	 * @param txtPrenom the txtPrenom to set
	 */
	public void setTxtPrenom(JTextField txtPrenom) {
		this.txtPrenom = txtPrenom;
	}

	/**
	 * @return the txtArrivingDate
	 */
	public JFormattedTextField getTxtArrivingDate() {
		return txtArrivingDate;
	}

	/**
	 * @param txtArrivingDate the txtArrivingDate to set
	 */
	public void setTxtArrivingDate(JFormattedTextField txtArrivingDate) {
		this.txtArrivingDate = txtArrivingDate;
	}

	/**
	 * @return the txtLeavingDate
	 */
	public JFormattedTextField getTxtLeavingDate() {
		return txtLeavingDate;
	}

	/**
	 * @param txtLeavingDate the txtLeavingDate to set
	 */
	public void setTxtLeavingDate(JFormattedTextField txtLeavingDate) {
		this.txtLeavingDate = txtLeavingDate;
	}

	/**
	 * @return the txtDepartment
	 */
	public JTextField getTxtDepartment() {
		return txtDepartment;
	}

	/**
	 * @param txtDepartment the txtDepartment to set
	 */
	public void setTxtDepartment(JTextField txtDepartment) {
		this.txtDepartment = txtDepartment;
	}

	/**
	 * @return the btnDelete
	 */
	public JButton getBtnDelete() {
		return btnDelete;
	}

	/**
	 * @param btnDelete the btnDelete to set
	 */
	public void setBtnDelete(JButton btnDelete) {
		this.btnDelete = btnDelete;
	}

	/**
	 * @return the btnClose
	 */
	public JButton getBtnClose() {
		return btnClose;
	}

	/**
	 * @param btnClose the btnClose to set
	 */
	public void setBtnClose(JButton btnClose) {
		this.btnClose = btnClose;
	}

	/**
	 * @return the btnUpdate
	 */
	public JButton getBtnUpdate() {
		return btnUpdate;
	}

	/**
	 * @param btnUpdate the btnUpdate to set
	 */
	public void setBtnUpdate(JButton btnUpdate) {
		this.btnUpdate = btnUpdate;
	}

	/**
	 * @return the lblDepartment
	 */
	public JLabel getLblDepartment() {
		return lblDepartment;
	}

	/**
	 * @param lblDepartment the lblDepartment to set
	 */
	public void setLblDepartment(JLabel lblDepartment) {
		this.lblDepartment = lblDepartment;
	}

	/**
	 * @return the lblLeavingDate
	 */
	public JLabel getLblLeavingDate() {
		return lblLeavingDate;
	}

	/**
	 * @param lblLeavingDate the lblLeavingDate to set
	 */
	public void setLblLeavingDate(JLabel lblLeavingDate) {
		this.lblLeavingDate = lblLeavingDate;
	}

	/**
	 * @return the lblDateArrive
	 */
	public JLabel getLblDateArrive() {
		return lblDateArrive;
	}

	/**
	 * @param lblDateArrive the lblDateArrive to set
	 */
	public void setLblDateArrive(JLabel lblDateArrive) {
		this.lblDateArrive = lblDateArrive;
	}

	/**
	 * @return the lblPrenom
	 */
	public JLabel getLblPrenom() {
		return lblPrenom;
	}

	/**
	 * @param lblPrenom the lblPrenom to set
	 */
	public void setLblPrenom(JLabel lblPrenom) {
		this.lblPrenom = lblPrenom;
	}

	/**
	 * @return the lblNom
	 */
	public JLabel getLblNom() {
		return lblNom;
	}

	/**
	 * @param lblNom the lblNom to set
	 */
	public void setLblNom(JLabel lblNom) {
		this.lblNom = lblNom;
	}

	/**
	 * @return the lblEmail
	 */
	public JLabel getLblEmail() {
		return lblEmail;
	}

	/**
	 * @param lblEmail the lblEmail to set
	 */
	public void setLblEmail(JLabel lblEmail) {
		this.lblEmail = lblEmail;
	}

	/**
	 * @return the txtEmail
	 */
	public JTextField getTxtEmail() {
		return txtEmail;
	}

	/**
	 * @param txtEmail the txtEmail to set
	 */
	public void setTxtEmail(JTextField txtEmail) {
		this.txtEmail = txtEmail;
	}

	/**
	 * @return the lblManager
	 */
	public JLabel getLblManager() {
		return lblManager;
	}

	/**
	 * @param lblManager the lblManager to set
	 */
	public void setLblManager(JLabel lblManager) {
		this.lblManager = lblManager;
	}

	/**
	 * @return the contentPanel
	 */
	public JPanel getContentPanel() {
		return contentPanel;
	}
	
}
