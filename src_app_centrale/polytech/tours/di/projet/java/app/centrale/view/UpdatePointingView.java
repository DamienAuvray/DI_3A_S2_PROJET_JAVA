package polytech.tours.di.projet.java.app.centrale.view;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JComboBox;

/**
 * UpdatePointingView
 * @author Damien Auvray
 *
 */
@SuppressWarnings("serial")
public class UpdatePointingView extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JLabel lblEmployee;
	private JComboBox<Object> cbEmployee;
	private JLabel lblDate;
	private JFormattedTextField txtDate;
	private JButton btnUpdate;
	private JButton btnClose;
	private JButton btnDelete;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UpdatePointingView dialog = new UpdatePointingView();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public UpdatePointingView() {
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		lblEmployee = new JLabel("Employee:");
		lblEmployee.setBounds(10, 14, 90, 14);
		contentPanel.add(lblEmployee);
		
		cbEmployee = new JComboBox<Object>();
		cbEmployee.setBounds(110, 11, 314, 20);
		contentPanel.add(cbEmployee);
		
		lblDate = new JLabel("Date:");
		lblDate.setBounds(10, 42, 90, 14);
		contentPanel.add(lblDate);
		
		txtDate = new JFormattedTextField();
		txtDate.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter(new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm"))));
		txtDate.setColumns(10);
		txtDate.setBounds(110, 39, 314, 20);
		contentPanel.add(txtDate);
		
		btnUpdate = new JButton("Update");
		btnUpdate.setBounds(236, 230, 89, 23);
		contentPanel.add(btnUpdate);
		
		btnClose = new JButton("Close");
		btnClose.setBounds(335, 230, 89, 23);
		contentPanel.add(btnClose);
		
		btnDelete = new JButton("Delete");
		btnDelete.setBounds(137, 230, 89, 23);
		contentPanel.add(btnDelete);
		
		setVisible(true);
	}

	/**
	 * @return the lblEmployee
	 */
	public JLabel getLblEmployee() {
		return lblEmployee;
	}

	/**
	 * @param lblEmployee the lblEmployee to set
	 */
	public void setLblEmployee(JLabel lblEmployee) {
		this.lblEmployee = lblEmployee;
	}

	/**
	 * @return the cbEmployee
	 */
	public JComboBox<Object> getCbEmployee() {
		return cbEmployee;
	}

	/**
	 * @param cbEmployee the cbEmployee to set
	 */
	public void setCbEmployee(JComboBox<Object> cbEmployee) {
		this.cbEmployee = cbEmployee;
	}

	/**
	 * @return the lblDate
	 */
	public JLabel getLblDate() {
		return lblDate;
	}

	/**
	 * @param lblDate the lblDate to set
	 */
	public void setLblDate(JLabel lblDate) {
		this.lblDate = lblDate;
	}

	/**
	 * @return the txtDate
	 */
	public JFormattedTextField getTxtDate() {
		return txtDate;
	}

	/**
	 * @param txtDate the txtDate to set
	 */
	public void setTxtDate(JFormattedTextField txtDate) {
		this.txtDate = txtDate;
	}

	/**
	 * @return the btnUpdate
	 */
	public JButton getBtnUpdate() {
		return btnUpdate;
	}

	/**
	 * @param btnUpdate the btnUpdate to set
	 */
	public void setBtnUpdate(JButton btnUpdate) {
		this.btnUpdate = btnUpdate;
	}

	/**
	 * @return the btnClose
	 */
	public JButton getBtnClose() {
		return btnClose;
	}

	/**
	 * @param btnClose the btnClose to set
	 */
	public void setBtnClose(JButton btnClose) {
		this.btnClose = btnClose;
	}

	/**
	 * @return the btnDelete
	 */
	public JButton getBtnDelete() {
		return btnDelete;
	}

	/**
	 * @param btnDelete the btnDelete to set
	 */
	public void setBtnDelete(JButton btnDelete) {
		this.btnDelete = btnDelete;
	}

	/**
	 * @return the contentPanel
	 */
	public JPanel getContentPanel() {
		return contentPanel;
	}
	
}
