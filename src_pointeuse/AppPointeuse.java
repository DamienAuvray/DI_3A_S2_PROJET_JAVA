import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

import polytech.tours.di.projet.java.app.centrale.model.config.Config;
import polytech.tours.di.projet.java.pointeuse.controller.*;
import polytech.tours.di.projet.java.pointeuse.model.pointeuse.Pointeuse;
import polytech.tours.di.projet.java.pointeuse.view.AppPointeuseView;

/**
 * AppPointeuse
 * @author Damien Auvray
 *
 */
public class AppPointeuse {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Pointeuse model = new Pointeuse();
		AppPointeuseView view = new AppPointeuseView();
		
		/**
		 * Appel methode pour get les employee depuis le JSON
		 */		
		try {
	    	ObjectInputStream ois = new ObjectInputStream(new FileInputStream("pointeuse.ser"));
			model = (Pointeuse) ois.readObject();
			ois.close();
		} catch (ClassNotFoundException e) {
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		}
		
		Config.update();
		
		//model.getPointings().removeAll(model.getPointings());
		//System.out.println(model.getPointings());
		AppPointeuseController c = new AppPointeuseController(model, view);
		c.initController();
	}

}
