package polytech.tours.di.projet.java.pointeuse.controller;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.JFrame;
import polytech.tours.di.projet.java.app.centrale.model.company.*;
import polytech.tours.di.projet.java.app.centrale.model.time.*;
import polytech.tours.di.projet.java.pointeuse.model.pointeuse.Pointeuse;
import polytech.tours.di.projet.java.pointeuse.model.tcp.TCPClientEmployee;
import polytech.tours.di.projet.java.pointeuse.model.tcp.TCPClientPointing;
import polytech.tours.di.projet.java.pointeuse.view.AppPointeuseView;

/**
 * AppPointeuseController
 * @author Damien Auvray
 *
 */
public class AppPointeuseController {
	
	private Pointeuse model;
	private AppPointeuseView view;
	
	/**
	 * getModel
	 * @return model
	 */
	public  Pointeuse getModel() {
		return model;
	}
	
	/**
	 * AppPointeuseController
	 * @param m
	 * @param v
	 */
	public AppPointeuseController( Pointeuse m, AppPointeuseView v){
		model = m;
		view = v;
		initView();
	}
	
	/**
	 * initView
	 */
	public void initView(){
		//init view
		clock();
		initEmployee();
		
		//SET CLOSE OPERATION
		view.getFrame().setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		view.getFrame().addWindowListener(new java.awt.event.WindowAdapter() {
		    @Override
		    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
		    	try {
		    		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("pointeuse.ser"));
			    	oos.writeObject(model);
			    	oos.close();
				} catch (FileNotFoundException e) {
				} catch (IOException e) {
				}
		    	System.exit(0);
		    }
		});
	}
	
	/**
	 * initController
	 */
	public void initController(){
		//init controller
		view.getBtnCheckInOut().addActionListener(e -> checkInOut());
		view.getBtnSyncApp().addActionListener(e -> syncApp());
	}

	/**
	 * initEmployee
	 */
	public void initEmployee(){
		view.getCbListEmp().removeAllItems();
		if(model.getEmployees()!=null){
			for(Employee m : model.getEmployees()){
				view.getCbListEmp().addItem(m.getName() + " " + m.getFirstName());
			}
		}
	}
	
	/**
	 * clock
	 */
	public void clock(){
		Thread clock = new Thread(){
			public void run(){
				try {
					for(;;){
						Date date = new Date();
						view.getLblDate().setText(date.getLdt().getMonth() + " " + date.getLdt().getDayOfMonth() + "th, " + date.getLdt().getYear());
						
				        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
						LocalDateTime now = LocalDateTime.now();
						view.getLblLocalDateTime().setText(now.format(formatter).toString() + " ... Let's say " + date.getLdt().format(formatter));
						sleep(1000);
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};
		clock.start();
	}
	
	/**
	 * checkInOut
	 */
	public void checkInOut(){
		String employee = (String) view.getCbListEmp().getSelectedItem();
		if(model.getEmployees()!=null){
			for(Employee emp: model.getEmployees()){
				if((emp.getName()+ " "+emp.getFirstName()).equals(employee))
					model.getPointings().add((new Pointing(emp)));
			}
		}
		if(model.getPointings()!=null)
			new Thread(new TCPClientPointing(model.getPointings())).start();
	}
	
	/**
	 * syncApp
	 */
	private void syncApp() {
		ExecutorService executor = Executors.newFixedThreadPool(1);
		try {
			model.setEmployees(executor.submit(new TCPClientEmployee()).get());
			initEmployee();
		} catch (InterruptedException e) {
		} catch (ExecutionException e) {
		}
	}
}

