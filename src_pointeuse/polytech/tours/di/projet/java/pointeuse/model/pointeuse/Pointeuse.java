package polytech.tours.di.projet.java.pointeuse.model.pointeuse;

import java.io.Serializable;
import java.util.ArrayList;

import polytech.tours.di.projet.java.app.centrale.model.company.Employee;
import polytech.tours.di.projet.java.app.centrale.model.time.Pointing;

/**
 * Pointeuse
 * @author Damien Auvray
 *
 */
@SuppressWarnings("serial")
public class Pointeuse implements Serializable{
	

	private ArrayList<Employee> employees = new ArrayList<Employee>();
	private ArrayList<Pointing> pointings = new ArrayList<Pointing>();
	
	/**
	 * getEmployees
	 * @return employees
	 */
	public ArrayList<Employee> getEmployees() {
		return employees;
	}
	
	/**
	 * setEmployees
	 * @param employees
	 */
	public void setEmployees(ArrayList<Employee> employees) {
		this.employees = employees;
	}
	
	/**
	 * getPointings
	 * @return pointings
	 */
	public ArrayList<Pointing> getPointings() {
		return pointings;
	}
	
	/**
	 * setPointings
	 * @param pointings
	 */
	public void setPointings(ArrayList<Pointing> pointings) {
		this.pointings = pointings;
	}
	
	/**
	 * toString
	 */
	@Override
	public String toString() {
		return "Pointeuse [employees=" + employees + ", pointings=" + pointings
				+ "]";
	}

}
