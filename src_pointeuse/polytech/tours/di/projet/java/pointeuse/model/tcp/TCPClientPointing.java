package polytech.tours.di.projet.java.pointeuse.model.tcp;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;

import polytech.tours.di.projet.java.app.centrale.model.config.Config;
import polytech.tours.di.projet.java.app.centrale.model.time.*;

/**
 * TCPClientPointing
 * @author Damien Auvray
 *
 */
public class TCPClientPointing implements Runnable{

	/** attributes */
	private Socket s; /** < the socket */
	private ObjectOutputStream oos; /** < the writing stream */
	private ArrayList<Pointing> pointings; /** < the pointing */
	
	//private AppPointeuseController model;

	/**
	 * the constructor
	 * @param p
	 */
	public TCPClientPointing(ArrayList<Pointing>pointings) { 		
		this.pointings = pointings;
		//this.model = model;
	} 
	
	/**
	 * the run method of the runnable interface
	 * set connection and write a poitning in the TCP Stream to the company
	 */
	@Override
	public void run() {
		System.out.println("\nTCPClientPointing launched");
		try { 
			s = new Socket(Config.getIpAdress(), Config.getPortServerPointing()); 
			oos = new ObjectOutputStream(s.getOutputStream());
			oos.writeObject(pointings);
			System.out.println("\nTCPClientPointing writing done.");
			pointings.removeAll(pointings);
			getS().close();
			oos.close();
		} catch(IOException e) 
		{ 
			System.out.println("\nwaiting for connection..."); 
		}
		
	}

	/**
	 * @return the s
	 */
	public Socket getS() {
		return s;
	}

	/**
	 * @param s the s to set
	 */
	public void setS(Socket s) {
		this.s = s;
	} 

}
