package polytech.tours.di.projet.java.pointeuse.view;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;

import java.awt.Font;

/**
 * AppPointeuseView
 * @author Damien Auvray
 *
 */
public class AppPointeuseView {

	private JFrame frame;
	private JLabel lblDate;
	private JLabel lblLocalDateTime;
	private JButton btnCheckInOut;
	private JComboBox<Object> cbListEmp;
	private JButton btnSyncApp;

	/**
	 * Initialize the contents of the frame.
	 */
	public AppPointeuseView() {
		frame = new JFrame();
		frame.setTitle("Time Tracker Emulator - V1");
		frame.setBounds(100, 100, 440, 290);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setResizable(false);
		
		btnCheckInOut = new JButton("Check in/out");
		btnCheckInOut.setBounds(297, 199, 109, 23);
		frame.getContentPane().add(btnCheckInOut);
		
		cbListEmp = new JComboBox<Object>();
		cbListEmp.setBounds(38, 200, 221, 22);
		frame.getContentPane().add(cbListEmp);
		
		btnSyncApp = new JButton("Sync. info from main app");
		btnSyncApp.setBounds(38, 154, 221, 23);
		frame.getContentPane().add(btnSyncApp);
		
		lblDate = new JLabel("Date");
		lblDate.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblDate.setBounds(172, 55, 125, 14);
		frame.getContentPane().add(lblDate);
		
		lblLocalDateTime = new JLabel("LocalDateTime");
		lblLocalDateTime.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblLocalDateTime.setBounds(108, 88, 241, 23);
		frame.getContentPane().add(lblLocalDateTime);

		frame.setVisible(true);
	}

	/**
	 * @return the frame
	 */
	public JFrame getFrame() {
		return frame;
	}

	/**
	 * @param frame the frame to set
	 */
	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	/**
	 * @return the lblDate
	 */
	public JLabel getLblDate() {
		return lblDate;
	}

	/**
	 * @param lblDate the lblDate to set
	 */
	public void setLblDate(JLabel lblDate) {
		this.lblDate = lblDate;
	}

	/**
	 * @return the lblLocalDateTime
	 */
	public JLabel getLblLocalDateTime() {
		return lblLocalDateTime;
	}

	/**
	 * @param lblLocalDateTime the lblLocalDateTime to set
	 */
	public void setLblLocalDateTime(JLabel lblLocalDateTime) {
		this.lblLocalDateTime = lblLocalDateTime;
	}

	/**
	 * @return the btnCheckInOut
	 */
	public JButton getBtnCheckInOut() {
		return btnCheckInOut;
	}

	/**
	 * @param btnCheckInOut the btnCheckInOut to set
	 */
	public void setBtnCheckInOut(JButton btnCheckInOut) {
		this.btnCheckInOut = btnCheckInOut;
	}

	/**
	 * @return the cbListEmp
	 */
	public JComboBox<Object> getCbListEmp() {
		return cbListEmp;
	}

	/**
	 * @param cbListEmp the cbListEmp to set
	 */
	public void setCbListEmp(JComboBox<Object> cbListEmp) {
		this.cbListEmp = cbListEmp;
	}

	/**
	 * @return the btnSyncApp
	 */
	public JButton getBtnSyncApp() {
		return btnSyncApp;
	}

	/**
	 * @param btnSyncApp the btnSyncApp to set
	 */
	public void setBtnSyncApp(JButton btnSyncApp) {
		this.btnSyncApp = btnSyncApp;
	}
	
}
